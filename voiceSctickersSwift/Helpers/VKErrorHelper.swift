//
//  VKErrorHelper.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 30/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

class VKErrorHelper: NSObject {
    class func errorFromResponse(response: [String: Any]) -> Error? {
        
        let errorData = response["error"] as? [String: Any]
        if errorData != nil {
            let error = NSError(domain: "vk_error", code: 777, userInfo: [NSLocalizedDescriptionKey:errorData!["error_msg"] as! String])
            return error
        }
        
        return nil
    }
}

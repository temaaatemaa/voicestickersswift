//
//  AlertHelper.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 16/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

typealias AlertButtonAction = (_:UIAlertAction)->()
class AlertHelper {
    
    class func alertOneButton(title: String,
                     message: String?,
                     buttonTitle: String) -> UIAlertController {
        return alertOneButton(title: title, message: message, buttonTitle: buttonTitle, buttonAction: { (_) in })
    }
    
    class func alertOneButton(title: String,
                     message: String?,
                     buttonTitle: String,
                     buttonAction: @escaping AlertButtonAction) -> UIAlertController {
        return alert(title: title,
                     message: message,
                     leftButtonTitle: buttonTitle,
                     leftButtonHandler: buttonAction)
    }
    
    class func alert(title: String,
                      message: String?,
                      leftButtonTitle: String,
                      rightButtonTitle: String? = nil,
                      leftButtonHandler: @escaping AlertButtonAction,
                      rightButtonHandler: @escaping AlertButtonAction = {_ in}) -> UIAlertController {
        return alertThreeButtons(title: title,
                                 message: message,
                                 firstButtonTitle: leftButtonTitle,
                                 secondButtonTitle: rightButtonTitle,
                                 firstButtonHandler: leftButtonHandler,
                                 secondButtonHandler: rightButtonHandler)
    }
    
    class func alertThreeButtons(title: String,
                                 message: String?,
                                 firstButtonTitle: String,
                                 secondButtonTitle: String?,
                                 thirdButtonTitle: String? = nil,
                                 firstButtonHandler: @escaping AlertButtonAction,
                                 secondButtonHandler:@escaping AlertButtonAction = {_ in},
                                 thirdButtonHandler: @escaping AlertButtonAction = {_ in}) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let firstAction = UIAlertAction.init(title: firstButtonTitle, style: .default, handler: firstButtonHandler)
        alert.addAction(firstAction)
        
        if let secondButtonTitle = secondButtonTitle {
            let secondAction = UIAlertAction.init(title: secondButtonTitle, style: .default, handler: secondButtonHandler)
            alert.addAction(secondAction)
        }
        
        if let thirdButtonTitle = thirdButtonTitle {
            let thirdAction = UIAlertAction.init(title: thirdButtonTitle, style: .default, handler: thirdButtonHandler)
            alert.addAction(thirdAction)
        }
        
        return alert
    }
    
    class func alertFor(error: Error, from viewController: UIViewController) {
        let alert = alertOneButton(title: "Ошибка!",
                                   message: error.localizedDescription,
                                   buttonTitle: "Ок")
        viewController.present(alert, animated: true)
    }
}

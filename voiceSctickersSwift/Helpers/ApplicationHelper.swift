//
//  ApplicationHelper.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 07/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

class ApplicationHelper {

    public func getApplicationVersion() -> String {
        let version = Bundle.main.infoDictionary!["CFBundleShortVersionString"]
        let build = Bundle.main.infoDictionary![kCFBundleVersionKey as String]
        
        return "\(version!)build\(build!)"
    }
    
}

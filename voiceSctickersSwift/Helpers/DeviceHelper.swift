//
//  DeviceHelper.swift
//  Sberbank
//
//  Created by Артем Заблудовский on 16/04/2019.
//  Copyright © 2019 Sberbank Technology. All rights reserved.
//

import UIKit

protocol DeviceHelperProtocol {
    static func getUid() -> String
    static func getVersion() -> String
}

class DeviceHelper: DeviceHelperProtocol {
    
    fileprivate var uidKey: String
    fileprivate var uid: String? {
        get {
            var id: String?
            if id == nil {
                id = DeviceHelper.value(keychainKey: uidKey, service: uidKey)
            }
            if id == nil {
                id = DeviceHelper.value(forUserDefaults: uidKey)
            }
            if id == nil {
                id = DeviceHelper.appleIFV()
            }
            if id == nil {
                id = DeviceHelper.randomUUID()
            }
            self.save(value: id!);
            return id;
        }
    }
    
    fileprivate init(key: String) {
        uidKey = key
    }
    
    static func getUid() -> String {
        return DeviceHelper(key: "SAFDevice").uid!
    }
    
    
    fileprivate static func keychainItem(key: String, service: String) -> NSMutableDictionary {
        let keychainItem = NSMutableDictionary()
        keychainItem[kSecClass] = kSecClassGenericPassword
        keychainItem[kSecAttrAccessible] = kSecAttrAccessibleAlways
        keychainItem[kSecAttrAccount] = key
        keychainItem[kSecAttrService] = service
        
        return keychainItem
    }
    
    fileprivate func save(value: String) {
        if DeviceHelper.value(forUserDefaults: self.uidKey) == nil {
            DeviceHelper.set(value: value, userDefaultsKey: self.uidKey)
        }
        if DeviceHelper.value(keychainKey: self.uidKey, service: self.uidKey) == nil {
            DeviceHelper.set(value: value, forKeychainKey: self.uidKey, inService: self.uidKey)
        }
    }

    fileprivate static func value(keychainKey:String, service:String) -> String? {
        var status: OSStatus
        let keychainItem = DeviceHelper.keychainItem(key: keychainKey, service: service)
        keychainItem[kSecReturnData] = kCFBooleanTrue;
        keychainItem[kSecReturnAttributes] = kCFBooleanTrue;
        var result: CFTypeRef?
        status = SecItemCopyMatching(keychainItem, &result)
        if status != noErr {
            return nil
        }
        let resultDict = result as! NSMutableDictionary
        let data = resultDict[kSecValueData]
        if data == nil {
            return nil
        }
        return String(data: data as! Data, encoding: String.Encoding.utf8)
    }
    
    fileprivate static func set(value: String, forKeychainKey key:String, inService service:String) {
        let keychainItem = DeviceHelper.keychainItem(key: key, service: service)
        keychainItem[kSecValueData] = value.data(using: String.Encoding.utf8)
        SecItemAdd(keychainItem, nil);
    }

    fileprivate static func value(forUserDefaults key: String) -> String? {
        return UserDefaults.standard.object(forKey: key) as? String
    }
    
    fileprivate static func set(value: String, userDefaultsKey key:String) {
        UserDefaults.standard.set(value, forKey: key)
        UserDefaults.standard.synchronize()
    }

    
    fileprivate static func appleIFV() -> String? {
        if (NSClassFromString("UIDevice") != nil) {
            return UIDevice.current.identifierForVendor?.uuidString
        }
        return nil
    }
    
    fileprivate static func randomUUID() -> String {

        let uuidRef = CFUUIDCreate(kCFAllocatorDefault)
        let cfuuid = CFUUIDCreateString(kCFAllocatorDefault, uuidRef)

        let uuid = cfuuid! as String
        
        return uuid
    }
    
    static func getVersion() -> String {
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        let build = dictionary["CFBundleVersion"] as! String
        return "\(version) build \(build)"
    }

}

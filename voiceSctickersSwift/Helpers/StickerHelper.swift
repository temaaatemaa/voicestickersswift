//
//  StickerHelper.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 17/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

class StickerHelper: NSObject {
    let stickerManager: StickerManagerProtocol = StickerManager()
    
    func isDownloadedAllStickersInAlbum(album: StickerAlbumProtocol) -> Bool {
        for sticker in album.stickers {
            if !stickerManager.isStickerDownloaded(sticker: sticker, from: album) {
                return false
            }
        }
        
        return true
    }
    
    func stickerSoundNumber(sticker: Sticker, from album: StickerAlbum) -> Int? {
        let stickers = album.stickers
        for (index, stickerInArray) in stickers.enumerated() {
            if sticker.name == stickerInArray.name {
                return index
            }
        }
        return nil
    }
    
    func isStickerFavourite(sticker: StickerProtocol, from album: StickerAlbumProtocol) -> Bool {
        let favouriteManager: FavouritesManagerProtocol = FavouritesManager()
        let favArray = favouriteManager.getFavouritesStickers()
        if favArray.count == 0 { return false }
        

        for fav in favArray {
            if fav.name == sticker.name {
                if album is StickerAlbum {
                    if fav.albumNumber == (album as! StickerAlbum).albumNumber {
                        return true
                    }
                } else if sticker is TestStickerProtocol {
                    if fav.albumNumber == (sticker as! TestStickerProtocol).albumNumber &&
                        fav.soundNumber == (sticker as! TestStickerProtocol).soundNumber {
                        return true
                    } else {
                        return false
                    }
                } else {
                    
                }
            }
        }
        
        return false
    }
    
    func searchSticker(from sticker: StickerProtocol, album: StickerAlbumProtocol) -> SearchStickerProtocol {
        let name = sticker.name
        var albumNumber: Int = 0
        var soundNumber: Int = 0
        let albumName = album.albumName
        var paid = false
        
        if let sticker = sticker as? TestStickerProtocol {
            albumNumber = sticker.albumNumber
            soundNumber = sticker.soundNumber
        } else if let album = album as? StickerAlbum {
            albumNumber = album.albumNumber
            soundNumber = stickerSoundNumber(sticker: sticker as! Sticker, from: album) ?? 0
            paid = album.paid
        }
        
        let searchSticker = SearchSticker(name: name,
                                          albumNumber: albumNumber,
                                          soundNumber: soundNumber,
                                          albumName: albumName,
                                          paid: paid)
        return searchSticker
    }
}

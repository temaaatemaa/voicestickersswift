//
//  StickerParser.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 07/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

class StickerParser: StickerParserProtocol {
    
    func parse(stickerData: Dictionary<String, Any>) -> Array<StickerAlbum> {
        var albumArray = Array<StickerAlbum>()
        
        for (albumName, albumInfo) in stickerData {
            if !albumName.contains("VoiceStickers") {
                let album = parse(albumInfo: albumInfo as! Dictionary<String, Any>, albumName: albumName)
                albumArray.append(album)
            }
        }
        
        return albumArray
    }
    
    func parse(albumInfo: Dictionary<String, Any>, albumName: String) -> StickerAlbum {
        let stickers = parse(stickersInfo: albumInfo["stickers"] as! Array<String>)
        let albumNumber = albumInfo["soundNumber"] as! Int
        let previewUrl = albumInfo["previewUrl"] as? String
        
        var paid = false
        if albumInfo["paid"] != nil {
            paid = albumInfo["paid"] as! String == "YES"
        }
        
        let album = StickerAlbum.init(albumName: albumName, albumNumber: albumNumber, albumPreviewUrl: previewUrl ?? "", paid: paid, stickers: stickers)
        
        return album
    }
    
    func parse(stickersInfo: Array<String>) -> Array<Sticker> {
        var stickersArray = Array<Sticker>()
        
        for stickerName in stickersInfo {
            stickersArray.append(Sticker.init(name: stickerName))
        }
        
        return stickersArray
    }
    
}

extension StickerParser {
    func parse(stickerTestData: Array<Dictionary<String, Any>>) -> Array<TestStickerAlbum> {
        var albumArray = Array<TestStickerAlbum>()
        
        for albumInfo in stickerTestData {
            let album = parse(albumTestInfo: albumInfo)
            albumArray.append(album)
        }
        
        return albumArray
    }
    
    func parse(albumTestInfo: Dictionary<String, Any>) -> TestStickerAlbum {
        let name = albumTestInfo["name"] as! String
        let previewUrl = albumTestInfo["previewUrl"] as! String
        var paid = false
        if albumTestInfo["paid"] != nil {
            paid = albumTestInfo["paid"] as! String == "YES"
        }
        
        let stickers = parse(stickersTestInfo: albumTestInfo["stickers"] as! Array<Dictionary<String, Any>>)
        
        return TestStickerAlbum.init(albumName: name, albumPreviewUrl: previewUrl, paid: paid, stickers: stickers)
    }
    
    func parse(stickersTestInfo: Array<Dictionary<String, Any>>) -> Array<TestSticker> {
        var stickersArray = Array<TestSticker>()
        
        for stickerInfo in stickersTestInfo {
            let name = stickerInfo["name"] as! String
            let albumNumber = stickerInfo["albumNumber"] as! Int
            let soundNumber = stickerInfo["soundNumber"] as! Int
            
            stickersArray.append(TestSticker.init(name: name, albumNumber: albumNumber, soundNumber: soundNumber))
        }
        
        return stickersArray
    }
}

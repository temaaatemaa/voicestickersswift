//
//  VKGroupParserProtocol.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 08/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

protocol VKGroupParserProtocol {
    func parse(groupData: Dictionary<String, Any>) -> VKGroup
    func parse(groupsData: Dictionary<String, Any>) -> Array<VKGroup>
    func parse(groupsData: Array<Any>) -> Array<VKGroup>
    func parse(groupId: Int, fromGroups groups: [Dictionary<String, Any>]) -> VKGroup? 
}

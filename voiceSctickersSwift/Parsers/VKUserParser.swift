//
//  VKUserParser.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 08/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

class VKUserParser: VKUserParserProtocol {
    
    func parse(userData: Dictionary<String, Any>) -> VKUser {
        let firstName = userData["first_name"] as? String ?? ""
        let lastName = userData["last_name"] as? String ?? ""
        let id = userData["id"] as? Int ?? 0
        let online = userData["online"] as? Bool ?? false
        let photoUrl = userData["photo_100"] as? String ?? ""
        
        let user = VKUser.init(firstName: firstName, lastName: lastName, online: online, photoUrl: photoUrl, id: id)
        return user
    }
    
    func parse(usersData: Dictionary<String, Any>) -> Array<VKUser> {
        var users = Array<VKUser>()
        
        let items = usersData["items"] as! Array<Dictionary<String, Any>>
        for item in items {
            let user = parse(userData: item)
            users.append(user)
        }
        
        return users
    }
    
    func parse(usersData: Array<Any>) -> Array<VKUser> {
        var users = Array<VKUser>()
        
        for item in usersData {
            let user = parse(userData: item as! Dictionary<String, Any>)
            users.append(user)
        }
        
        return users
    }
    
    func parse(userId: Int, fromProfiles profiles: [Dictionary<String, Any>]) -> VKUser? {
        for profile in profiles {
            let profileId = profile["id"] as! Int
            if profileId ==  userId {
                let user = VKUser.init(firstName: profile["first_name"] as? String,
                                       lastName: profile["last_name"] as? String,
                                       online: profile["online"] as? Bool ?? false,
                                       photoUrl: profile["photo_100"] as? String,
                                       id: userId)
                return user
            }
        }
        return nil
    }
}

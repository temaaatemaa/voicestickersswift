//
//  VKUserParserProtocol.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 08/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

protocol VKUserParserProtocol {
    func parse(userData: Dictionary<String, Any>) -> VKUser
    func parse(usersData: Dictionary<String, Any>) -> Array<VKUser>
    func parse(usersData: Array<Any>) -> Array<VKUser>
    func parse(userId: Int, fromProfiles profiles: [Dictionary<String, Any>]) -> VKUser?
}

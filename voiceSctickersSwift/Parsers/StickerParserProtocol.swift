//
//  StickerParserProtocol.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 07/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

protocol StickerParserProtocol {
    func parse(stickerData: Dictionary<String, Any>) -> Array<StickerAlbum>
    func parse(stickerTestData: Array<Dictionary<String, Any>>) -> Array<TestStickerAlbum>
}

//
//  VKChatParserProtocol.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 27/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

protocol VKChatParserProtocol {
    func parse(chatId: Int, from conversationData: [String: Any]) -> VKChat 
}

//
//  VKDialogParserProtocol.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 08/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

protocol VKDialogParserProtocol {
    func parse(dialogData: Dictionary<String, Any>,
               profilesData: [Dictionary<String, Any>]?,
               groupsData: [Dictionary<String, Any>]?) -> VKDialog?
    func parse(dialogsData: Dictionary<String, Any>) -> Array<VKDialog>
}

//
//  VKGroupParser.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 08/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

class VKGroupParser: VKGroupParserProtocol {
    
    func parse(groupData: Dictionary<String, Any>) -> VKGroup {
        let name = groupData["name"] as! String
        let id = groupData["id"] as! Int
        let photoUrl = groupData["photo_100"] as! String
        
        let group = VKGroup.init(id: id, name: name, photoUrl: photoUrl)
        return group
    }
    
    func parse(groupsData: Dictionary<String, Any>) -> Array<VKGroup> {
        var groups = Array<VKGroup>()
        
        let items = groupsData["items"] as! Array<Dictionary<String, Any>>
        for item in items {
            let group = parse(groupData: item)
            groups.append(group)
        }
        
        return groups
    }
    
    func parse(groupsData: Array<Any>) -> Array<VKGroup> {
        var groups = Array<VKGroup>()
        
        for item in groupsData {
            let group = parse(groupData: item as! Dictionary<String, Any>)
            groups.append(group)
        }
        
        return groups
    }
    
    func parse(groupId: Int, fromGroups groups: [Dictionary<String, Any>]) -> VKGroup? {
        for groupData in groups {
            let groupDataId = groupData["id"] as! Int
            if groupDataId ==  -groupId {
                let group = VKGroup(id: groupId,
                                    name: groupData["name"] as? String,
                                    photoUrl: groupData["photo_100"] as? String)
                return group
            }
        }
        return nil
    }
}

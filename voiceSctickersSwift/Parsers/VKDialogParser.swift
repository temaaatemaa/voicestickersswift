//
//  VKDialogParser.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 08/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

class VKDialogParser: VKDialogParserProtocol {
    let userParser: VKUserParserProtocol = VKUserParser()
    let groupParser: VKGroupParserProtocol = VKGroupParser()
    let chatParser: VKChatParserProtocol = VKChatParser()
    
    func parse(dialogData: Dictionary<String, Any>,
               profilesData: [Dictionary<String, Any>]?,
               groupsData: [Dictionary<String, Any>]?) -> VKDialog? {
        
        let messageData = dialogData["last_message"] as! Dictionary<String, Any>
        let message = messageData["text"] as? String
        
        let conversationData = dialogData["conversation"] as! Dictionary<String, Any>
        let peer = conversationData["peer"] as! Dictionary<String, Any>
        let userId = peer["id"] as! Int
        let type = peer["type"] as! String
        
        var member: VKReceiverProtocol?
        if type == "chat" {
            member = chatParser.parse(chatId: userId, from: conversationData)
        } else if type == "user" {
            if let profilesData = profilesData {
                member = userParser.parse(userId: userId, fromProfiles: profilesData)!
            }
        } else {
            if let groupsData = groupsData {
                member = groupParser.parse(groupId: userId, fromGroups: groupsData)!
            }
        }
        
        if member == nil {
            return nil
        }
        
        let dialog = VKDialog.init(message: message, member: member)
        return dialog
    }
    
    func parse(dialogsData: Dictionary<String, Any>) -> Array<VKDialog> {
        var dialogs = Array<VKDialog>()
        print(dialogsData)
        Reports.logEvent("parse_dialogsData", parameters: dialogsData)
        let profiles = dialogsData["profiles"] as? [Dictionary<String, Any>]
        let groups = dialogsData["groups"] as? [Dictionary<String, Any>]
        let items = dialogsData["items"] as? Array<Dictionary<String, Any>>
        
        if items == nil {
            return dialogs
        }
        
        for item in items! {
            let dialog = parse(dialogData: item, profilesData: profiles, groupsData: groups)
            if let dialog = dialog {
                dialogs.append(dialog)
            }
        }
        
        return dialogs
    }
}

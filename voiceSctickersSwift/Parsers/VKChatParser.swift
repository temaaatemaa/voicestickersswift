//
//  VKChatParser.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 27/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

class VKChatParser: VKChatParserProtocol {
    func parse(chatId: Int, from conversationData: [String: Any]) -> VKChat {
        let settings = conversationData["chat_settings"] as! Dictionary<String, Any>
        let title = settings["title"] as? String
        var photo: String?
        let photosData = settings["photo"] as? Dictionary<String, Any>
        if let photosData = photosData {
            photo = photosData["photo_100"] as? String
        }
        return VKChat.init(id: chatId, name: title, photoUrl: photo)
    }
}

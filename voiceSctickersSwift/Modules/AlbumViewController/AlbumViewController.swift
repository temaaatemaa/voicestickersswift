//
//  AlbumViewController.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 07/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit
import SDWebImage
import Firebase

class AlbumViewController: UIViewController {
    
    
    var output: AlbumViewOutput!
    
    
    
    let sendViewTopOffset = 10 as CGFloat
    let sendViewBottomOffset = 10 as CGFloat
    let sendViewHeight = UIScreen.main.bounds.height * 0.1
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var albumImageView: UIImageView!
    @IBOutlet weak var donloadAlbumButton: UIButton!
    
    var sendView: SendView?
    
    var album: StickerAlbumProtocol?
    
    var selectedIndexPath: IndexPath?
    var allDownloading: Bool = false
    
    let cahedManager = ImageCacheManager()
    let stickerHelper = StickerHelper()
    
    
    // MARK: - LIFECYCLE
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }
    
    override func viewWillAppear(_ animated: Bool) {
		output.viewWillAppear()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        output.viewWillDissapear()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        donloadAlbumButton.layer.cornerRadius = donloadAlbumButton.frame.width / 2
    }
    
    func initialSetup() {
        setupNavigationBar()
        setupCollectionView()

        let placeHolder = UIImage(named: "placeholder")
        let cachedImage = cahedManager.getImage(for: (album?.albumPreviewUrl)!)
        if cachedImage == nil {
            albumImageView.sd_setImage(with: URL(string: (album?.albumPreviewUrl)!)!, placeholderImage: placeHolder, options: .highPriority) { (image, error, cache, url) in
                self.cahedManager.saveImage(image: image, error: error, cache: cache, url: url)
            }
        } else {
            albumImageView.image = cachedImage
        }
    }
    
    func setupNavigationBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
    }
    
    func setupCollectionView() {
        collectionView.contentInset = UIEdgeInsets(top: 5, left: 5, bottom: sendViewHeight + sendViewTopOffset + sendViewBottomOffset, right: 5)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.registerCell(cell: AlbumCollectionViewCell.self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toChoose" {
            showChooseVkReceiverModule(module: segue.destination as! ChooseVKReceiverModuleProtocol)
        } else if segue.identifier == "toAudioChange" {
            showChangeAudioModule(module: segue.destination as! ChangeAudioModuleProtocol)
        }
    }
    
    func showChooseVkReceiverModule(module: ChooseVKReceiverModuleProtocol) {
        output.didTapSendVk()
    }
    
    func showChangeAudioModule(module: ChangeAudioModuleProtocol) {
        output.didTapChangeSticker()
    }
}

extension AlbumViewController: AlbumViewInput {
    
    func setupInitialState() {
        initialSetup()
    }
    
    func showLoadingState(loading: Bool, for sticker: StickerProtocol) {
        let index = indexFor(sticker: sticker)
        let cell = self.collectionView.cellForItem(at: IndexPath(item: index!, section: 0))
        if let cell = cell as? AlbumCollectionViewCell {
            cell.update(isloading: loading)
        }
    }
    
    func showDownloadedState(downloaded: Bool, for sticker: StickerProtocol) {
        let index = indexFor(sticker: sticker)
        let cell = self.collectionView.cellForItem(at: IndexPath(item: index!, section: 0))
        if let cell = cell as? AlbumCollectionViewCell {
            cell.update(didDownloade: downloaded)
        }
    }
    
    func update(with album: StickerAlbumProtocol) {
        self.album = album
		collectionView.reloadData()
    }
}

// MARK: - Button Actions


extension AlbumViewController {
    @IBAction func didTapDownloadAlbumButton(_ sender: Any) {
        allDownloading = true
        collectionView.reloadData()
        output.didTapDownloadAlbum()
    }
    
    func indexFor(sticker: StickerProtocol) -> Int? {
        return self.album?.stickers.firstIndex(where: { (tmpSticker) -> Bool in
            return sticker.name == tmpSticker.name
        })
    }
    
    
    func hideAlbumDownloadButton() {
        self.allDownloading = false
        self.collectionView.reloadData()
        self.donloadAlbumButton.isHidden = true
    }
    
    func showAlbumDownloadButton() {
        let downloadIcon = donloadAlbumButton.image(for: .normal)?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        donloadAlbumButton.setImage(downloadIcon, for: .normal)
        donloadAlbumButton.backgroundColor = UIColor(rgb: 0x34495e)
        donloadAlbumButton.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
}


// MARK: CollectionViewDelegates


extension AlbumViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return album?.stickers.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AlbumCollectionViewCell.reuseIdentifierString, for: indexPath) as! AlbumCollectionViewCell
        cell.delegate = self
        let sticker = album?.stickers[indexPath.row]
        cell.nameLabel.text = sticker!.name
        
        let isDownloadedSticker = output.isStickerDownloaded(sticker: sticker!)
        cell.update(didDownloade: isDownloadedSticker)
        
        let isLoading = !isDownloadedSticker && allDownloading
        cell.update(isloading: isLoading)
        
        cell.update(selected: indexPath == selectedIndexPath)
        
        cell.update(favourite: stickerHelper.isStickerFavourite(sticker: sticker!, from: album!))

		if sticker is OwnSticker {
			cell.favouritesButton.isHidden = true
		}

        return cell
    }
}

extension AlbumViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        unselectPreviousCell()
        
        selectedIndexPath = indexPath
        let cell = collectionView.cellForItem(at: indexPath) as! AlbumCollectionViewCell
        cell.update(selected: true)
        
        let sticker = (album?.stickers[indexPath.row])!
        
        output.didSelectSticker(sticker: sticker)
    }
    
    func unselectPreviousCell() {
        if selectedIndexPath != nil {
            let prevSelectedCell = collectionView.cellForItem(at: selectedIndexPath!)
            if let cell = prevSelectedCell as? AlbumCollectionViewCell {
                cell.update(selected: false)
            }
        }
    }
    
    func showSendButton() {
        if sendView != nil {
            return
        }
        DispatchQueue.main.async { [weak self] in
            self?.sendView = SendView.initFromNib(with: self!)
            self?.sendView?.alpha = 0.02
			if self?.album is OwnStickerAlbum {
				self?.sendView?.changeButton.setImage(#imageLiteral(resourceName: "close"), for: .normal)
			}
            self?.view.addSubview((self?.sendView!)!)
            let heightSatus = UIApplication.shared.statusBarFrame.height
            self?.sendView?.frame = CGRect(x: 10,
                                     y: self!.view.frame.height - self!.tabBarController!.tabBar.frame.size.height - self!.sendViewBottomOffset - self!.navigationController!.navigationBar.frame.size.height - heightSatus,
                                     width: self!.view.frame.width - 10 * 2,
                                     height: self!.sendViewHeight)
            UIView.animate(withDuration: 0.5) {
                self?.sendView?.alpha = 1
            }
        }
    }
}

extension AlbumViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemsInRow = 3 as CGFloat
        let offset = 5 as CGFloat
        let screenWidth = UIScreen.main.bounds.width
        let itemWidth = (screenWidth - offset * (itemsInRow - 1) - collectionView.contentInset.left - collectionView.contentInset.right) / itemsInRow
        return CGSize(width: itemWidth, height: itemWidth)
    }
}


// MARK: - SendViewDelegate


extension AlbumViewController: SendViewDelegate {
    func didTapVkButton() {
        output.didTapSendVk()
    }
    
    func didTapTelegramButton() {
        output.didTapSendTelegram()
    }
    
    func didTapChangeButton() {
		if self.album is OwnStickerAlbum {
			output.didTapDeleteSticker()
			return
		}
        output.didTapChangeSticker()
    }
}


extension AlbumViewController: AlbumCollectionViewCellProtocol {
    func albumCollectionViewCell(cell: AlbumCollectionViewCell, didTapFavouritesButton button: UIButton) {
        let indexPath = self.collectionView.indexPath(for: cell)
        let sticker = album?.stickers[(indexPath?.item)!]
        output.didTapFavouritesButton(sticker: sticker!)
        collectionView.reloadData()
    }
}

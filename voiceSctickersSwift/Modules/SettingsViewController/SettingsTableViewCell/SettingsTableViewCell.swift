//
//  SettingsTableViewCell.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 16/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    var initColor: UIColor?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initColor = containerView.backgroundColor
        let selectedView = UIView()
        selectedView.backgroundColor = UIColor(rgb: 0x0058DF)
        selectedView.frame = containerView.frame
        selectedBackgroundView = selectedView
    }
    override func layoutSubviews() {
        selectedBackgroundView?.frame = containerView.frame
        selectedBackgroundView?.layer.cornerRadius = containerView.layer.cornerRadius
    }
}

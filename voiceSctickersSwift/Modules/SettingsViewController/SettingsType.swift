//
//  SettingsType.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 21/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

enum SettingType: String {
    case vkAccount = "Текущий аккаунт"
    case exitVkAccount = "Выйти из аккаунта"
    case rateApp = "Оценить в App Store"
    case about = "О приложении"
    case feedback = "Обратная связь"
    case buy = "Приобрести VIP статус"
    case restore = "Восстановить покупки"
    case design = "Помочь с дизайном"
    
    static func array() -> Array<SettingType> {
        return [.vkAccount, .exitVkAccount, .rateApp, .about, .feedback, .buy, .restore, .design]
    }
}

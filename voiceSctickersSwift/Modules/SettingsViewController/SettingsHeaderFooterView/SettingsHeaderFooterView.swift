//
//  SettingsHeaderFooterView.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 17/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

class SettingsHeaderFooterView: UITableViewHeaderFooterView {
    @IBOutlet weak var captureLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
}

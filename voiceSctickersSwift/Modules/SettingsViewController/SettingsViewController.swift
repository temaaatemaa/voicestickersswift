//
//  SettingsViewController.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 16/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit
import Firebase

class SettingsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    let vkService: VKServiceProtocol = VKService()
    let applicationHelper = ApplicationHelper()
    let paymentManager: PaymentManagerProtocol = PaymentManager()
    
    var vkName: String?
    var settingsArray = SettingType.array()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.isNavigationBarHidden = true
        
        setupTableView()
        requestForName()
        
        if !vkService.isLogin {
            removeButton(type: .vkAccount)
        }
        
        if paymentManager.isBuy {
            self.removeButton(type: .buy)
            self.removeButton(type: .restore)
        }
        
        tableView.reloadData()
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(PaymentManagerVipDidBuy), object: nil, queue: OperationQueue.main) { (_) in
            self.removeButton(type: .buy)
            self.removeButton(type: .restore)
            self.tableView.reloadData()
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(PaymentManagerVipDidBuy), object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        requestForName()
        self.tableView.reloadData()
    }
    
    func setupTableView() {
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.registerCell(cell: NameSettingsTableViewCell.self)
        tableView.registerCell(cell: SettingsTableViewCell.self)
        tableView.register(UINib(nibName: String(describing: SettingsHeaderFooterView.self), bundle: nil),
                           forHeaderFooterViewReuseIdentifier: String(describing: SettingsHeaderFooterView.self))
    }
}


// MARK: - TableViewDelegates


extension SettingsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settingsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell
        switch settingsArray[indexPath.row] {
        case .vkAccount:
            let nameCell = tableView.dequeueReusableCell(withIdentifier: NameSettingsTableViewCell.reuseIdentifierString) as! NameSettingsTableViewCell
            nameCell.nameLabel.text = vkName
            cell = nameCell
            break;
        case .exitVkAccount:
            let exitCell = tableView.dequeueReusableCell(withIdentifier: SettingsTableViewCell.reuseIdentifierString) as! SettingsTableViewCell
            exitCell.nameLabel.text = vkService.isLogin ? settingsArray[indexPath.row].rawValue : "Войти в аккаунт"
            cell = exitCell
            break
//        case .rateApp:
//        case .about:
//        case .feedback:
//        case .buy:
//        case .restore:
        default:
            let settingsCell = tableView.dequeueReusableCell(withIdentifier: SettingsTableViewCell.reuseIdentifierString) as! SettingsTableViewCell
            settingsCell.nameLabel.text = settingsArray[indexPath.row].rawValue
            cell = settingsCell
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 123
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: String(describing: SettingsHeaderFooterView.self)) as! SettingsHeaderFooterView
        view.captureLabel?.text = "Настройки"
        view.iconImageView.image = UIImage(named: "change-logo")
        return view
    }
}

extension SettingsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch settingsArray[indexPath.row] {
        case .vkAccount:
            return
        case .exitVkAccount:
            didTapExitVkAccountSetting()
            Reports.logEvent("settings_login_button", parameters: nil)
            return
        case .rateApp:
            AlertManager.showRateAlert(from: self)
            Reports.logEvent("settings_rate", parameters: nil)
            break;
        case .about:
            AlertManager.showAboutAlert(from: self)
            Reports.logEvent("settings_about", parameters: nil)
            break;
        case .feedback:
            AlertManager.showFeedbackAlert(from: self)
            Reports.logEvent("settings_feedback", parameters: nil)
            break;
        case .buy:
            buyOrRestore()
            Reports.logEvent("settings_buy_button", parameters: nil)
            break;
        case .restore:
            buyOrRestore(actionString: "restore")
            Reports.logEvent("settings_restore_button", parameters: nil)
            break;
        case .design:
            AlertManager.showDesignAlert(from: self)
            Reports.logEvent("settings_design_button", parameters: nil)
            break
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: - Private Methods

extension SettingsViewController {
    func indexFor(settingsType: SettingType) -> Int? {
        for (index, setting) in settingsArray.enumerated() {
            if setting == settingsType {
                return index
            }
        }
        
        return nil
    }
    
    func removeButton(type: SettingType) {
        let index = self.indexFor(settingsType: type)
        if index != nil {
            self.settingsArray.remove(at: index!)
        }
    }
    func buyOrRestore(actionString: String = "buy") {
        let hud = Hud.loading
        hud.show(in: view)
        let action: (_ error: Error?) -> () = {(error) in
            hud.dismiss()
            if let error = error {
                AlertHelper.alertFor(error: error, from: self)
                Reports.logError("settings_\(actionString)_error", error: error)
            } else {
                Reports.logEvent("settings_\(actionString)", parameters: nil)
                self.removeButton(type: .buy)
                self.removeButton(type: .restore)
                self.tableView.reloadData()
            }
        }
        if actionString == "buy" {
            paymentManager.buy(complitionHandler: action)
        } else {
            paymentManager.restore(complitionHandler: action)
        }
    }
    
    func didTapExitVkAccountSetting() {
        vkService.isLogin ? vkService.logout() : vkService.autentification(from: self)
        if settingsArray.first == SettingType.vkAccount && !vkService.isLogin {
            removeButton(type: .vkAccount)
        }
        requestForName()
        tableView.reloadData()
    }
    
    fileprivate func requestForName() {
        if vkService.isLogin {
            if !(settingsArray.first == SettingType.vkAccount) {
                settingsArray.insert(SettingType.vkAccount, at: 0)
            }
            vkService.getUserInfo { (name, surname, error) in
                if let error = error {
                    AlertHelper.alertFor(error: error, from: self)
                } else {
                    let name = name ?? ""
                    let surname = surname ?? ""
                    self.vkName = name  + " " + surname
                    self.tableView.reloadData()
                }
            }
        }
    }
}

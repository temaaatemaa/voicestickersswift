//
//  NameSettingsTableViewCell.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 17/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

class NameSettingsTableViewCell: UITableViewCell {

    @IBOutlet weak var captureLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

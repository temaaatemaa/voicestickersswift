//
//  DreamHeaderFooterView.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 20/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

protocol DreamHeaderFooterViewProtocol {
    func didTapAddButton()
}

class DreamHeaderFooterView: UITableViewHeaderFooterView {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addButton: UIButton!
    
    var delegate: DreamHeaderFooterViewProtocol?
    
    @IBAction func didTapAddButton(_ sender: Any) {
        delegate?.didTapAddButton()
    }
}

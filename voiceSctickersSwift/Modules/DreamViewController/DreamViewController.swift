//
//  ViewController.swift
//  Dreams
//
//  Created by Nik on 07.05.2019.
//  Copyright © 2019 Nik. All rights reserved.
//

import UIKit
import Firebase

class DreamViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    let dreamManager: DreamManagerProtocol = DreamManager()
    var dreams: [Dream]?
    

    override func viewDidLoad() {
        super.viewDidLoad()
    
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: String(describing: DreamHeaderFooterView.self), bundle: nil), forHeaderFooterViewReuseIdentifier: String(describing: DreamHeaderFooterView.self))
        tableView.registerCell(cell: DreamTableViewCell.self)
        
        requestForDreams()
    }
    
    func  alertVc() {
        let alertVC = UIAlertController(title: "Кого не хватает в приложении?", message: "У вас осталось сегодня желаний: \(self.dreamManager.newStickersRemaining)", preferredStyle: .alert)
        
        let alertNo = UIAlertAction(title: "Назад", style: .cancel, handler: nil)
        
        alertVC.addTextField { (textField) in
            textField.placeholder = "Стикер"
        }
        let alertOk = UIAlertAction(title: "Добавить", style: .default, handler: { [weak alertVC] (_) in
            let text = alertVC?.textFields![0].text
            let hud = Hud.loading
            hud.show(in: self.view)
            self.dreamManager.addDream(name: text!, completionHandler: { (error) in
                hud.dismiss()
                if let error = error {
                    AlertHelper.alertFor(error: error, from: self)
                } else {
                    let alert = AlertHelper.alert(title: "Успешно добавлено", message: "" , leftButtonTitle: "Ок", rightButtonTitle: nil, leftButtonHandler: { (alert) in }, rightButtonHandler: { (alert) in})
                    self.present(alert, animated: true)
                    self.requestForDreams()
                }
            })
        })
        alertVC.addAction(alertNo)
        alertVC.addAction(alertOk)
      
        
        self.present(alertVC, animated: true, completion: nil)
    }
    
    func requestForDreams() {
        let hud = Hud.loading
        hud.show(in: view)
        dreamManager.getDreams { (dreams, error) in
            hud.dismiss()
            if let error = error {
                AlertHelper.alertFor(error: error, from: self)
            } else {
                self.dreams = dreams
                self.tableView.reloadData()
            }
        }
    }
}

extension DreamViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dreams?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DreamTableViewCell.reuseIdentifierString) as! DreamTableViewCell
        let dream = dreams![indexPath.row]
        cell.stickerLabel?.text = dream.name
        cell.countLabel.text = String((dream.votedCount)!)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 123
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: String(describing: DreamHeaderFooterView.self)) as! DreamHeaderFooterView
        view.nameLabel.text = "Желания"
        view.delegate = self
        
        return view
    }
}

extension DreamViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if dreamManager.votesRemaining == 0 {
            AlertManager.showNoVotesAlert(from: self)
            return
        }
        let hud = Hud.loading
        hud.show(in: view)
        let dream = dreams![indexPath.row]
        dreamManager.voteFor(dream: dream) { (error) in
            hud.dismiss()
            if let error = error {
                AlertHelper.alertFor(error: error, from: self)
                Reports.logError("dream_vote_error", error: error)
            } else {
                self.requestForDreams()
                let alert = AlertHelper.alert(title: "Ваш голос учтен!", message: "Осталось голосов сегодня: \(self.dreamManager.votesRemaining)" , leftButtonTitle: "Ок", rightButtonTitle: nil, leftButtonHandler: { (alert) in }, rightButtonHandler: { (alert) in})
                self.present(alert, animated: true)
                Reports.logEvent("dream_vote", parameters: ["sticker": dream.name ?? ""])
            }
        }
        Reports.logEvent("dream_vote_button", parameters: ["sticker": dream.name ?? ""])
    }
}

extension DreamViewController: DreamHeaderFooterViewProtocol {
    func didTapAddButton() {
        if dreamManager.newStickersRemaining == 0 {
            AlertManager.showNoVotesAlert(from: self)
            return
        }
        alertVc()
    }
}

//
//  AlbumAlbumViewInput.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovky on 22/05/2019.
//  Copyright © 2019 Zabludovskiy. All rights reserved.
//

protocol AlbumViewInput: class {

    func setupInitialState()
    func update(with album: StickerAlbumProtocol)
    func showSendButton()
    func showLoadingState(loading: Bool, for sticker: StickerProtocol)
    func showDownloadedState(downloaded: Bool, for sticker: StickerProtocol)
    func hideAlbumDownloadButton()
    func showAlbumDownloadButton()
}

//
//  AlbumCollectionViewCell.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 16/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit
import JGProgressHUD

protocol AlbumCollectionViewCellProtocol {
    func albumCollectionViewCell(cell: AlbumCollectionViewCell, didTapFavouritesButton button: UIButton)
}

class AlbumCollectionViewCell: UICollectionViewCell {
    
    var delegate: AlbumCollectionViewCellProtocol?

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var favouritesButton: UIButton!
    var hud: JGProgressHUD?
    
    let downloadedColor = UIColor(rgb: 0x3498db)
    let notDownloadedColor = UIColor(rgb: 0x2980b9)
    
    let selectedColor = UIColor(rgb: 0x2f8dcb)
    
    override func awakeFromNib() {
        super.awakeFromNib()

        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 4.0)
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 0.3
        self.layer.masksToBounds = false
        
        iconImageView.image = iconImageView.image?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath
    }
    
    func update(isloading: Bool) {
        if hud == nil {
            self.hud = Hud.loading
            self.hud?.contentInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
            self.hud?.layoutMargins = UIEdgeInsets.zero
        }
        if isloading {
            self.hud?.show(in: self.contentView)
        } else {
            self.hud?.dismiss()
        }
    }
    
    func update(didDownloade: Bool) {
        iconImageView.isHidden = didDownloade
        contentView.backgroundColor = didDownloade ? downloadedColor : notDownloadedColor
    }
    
    func update(selected: Bool) {
        contentView.layer.borderWidth = selected ? 2 : 0
        contentView.layer.borderColor = UIColor(rgb: 0xe74c3c).cgColor
    }
    
    func update(favourite: Bool) {
        if favourite {
            favouritesButton.setImage(UIImage(named: "favourite"), for: .normal)
        } else {
            favouritesButton.setImage(UIImage(named: "unfavourite"), for: .normal)
        }
    }

    @IBAction func didTapFavouritesButton(_ sender: Any) {
        delegate?.albumCollectionViewCell(cell: self, didTapFavouritesButton: sender as! UIButton)
    }
}

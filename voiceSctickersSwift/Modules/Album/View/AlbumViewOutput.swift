//
//  AlbumAlbumViewOutput.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovky on 22/05/2019.
//  Copyright © 2019 Zabludovskiy. All rights reserved.
//

protocol AlbumViewOutput {

    func viewIsReady()
	func viewWillAppear()
    func viewWillDissapear()
    func didTapDownloadAlbum()
    func didSelectSticker(sticker: StickerProtocol)
    func didTapSendVk()
    func didTapSendTelegram()
    func didTapChangeSticker()
    func isStickerDownloaded(sticker: StickerProtocol) -> Bool
    func didTapFavouritesButton(sticker: StickerProtocol)
	func didTapDeleteSticker()
}

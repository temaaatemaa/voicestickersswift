//
//  AlbumAlbumInitializer.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovky on 22/05/2019.
//  Copyright © 2019 Zabludovskiy. All rights reserved.
//

import UIKit

class AlbumModuleInitializer: NSObject {

    @IBOutlet weak var albumViewController: AlbumViewController!

    override func awakeFromNib() {

        let configurator = AlbumModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: albumViewController)
    }

}

//
//  AlbumAlbumConfigurator.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovky on 22/05/2019.
//  Copyright © 2019 Zabludovskiy. All rights reserved.
//

import UIKit

class AlbumModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? AlbumViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: AlbumViewController) {

        let router = AlbumRouter()

        let presenter = AlbumPresenter()
        presenter.view = viewController
        presenter.router = router
        presenter.vkService = VKService()

        let stickerManager = StickerManager()
        
        let interactor = AlbumInteractor()
        interactor.output = presenter
        interactor.stickerManager = stickerManager

        presenter.interactor = interactor
        viewController.output = presenter

        router.view = viewController
        router.vkService = VKService()
        router.stickerManager = stickerManager
        router.telegramSendManager = TelegramSenderManager()
    }

}

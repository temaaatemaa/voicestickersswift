//
//  AlbumAlbumInteractorInput.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovky on 22/05/2019.
//  Copyright © 2019 Zabludovskiy. All rights reserved.
//

import Foundation

protocol AlbumInteractorInput: BaseInteractorInputProtocol {
	var updatedOwnStickersAlbum: OwnStickerAlbum? { get }
    func isStickerDownloaded(sticker: StickerProtocol, from album: StickerAlbumProtocol) -> Bool
    func downloadSticker(sticker: StickerProtocol, from album: StickerAlbumProtocol)
    func downloadAlbum(album: StickerAlbumProtocol)
    func stopPlay()
    
    func addFavourites(sticker: StickerProtocol, from album: StickerAlbumProtocol)
	func deleteSticker(ownSticker: OwnSticker)
}

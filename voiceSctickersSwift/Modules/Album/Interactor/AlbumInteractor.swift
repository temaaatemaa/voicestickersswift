//
//  AlbumAlbumInteractor.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovky on 22/05/2019.
//  Copyright © 2019 Zabludovskiy. All rights reserved.
//

class AlbumInteractor: BaseInteractor, AlbumInteractorInput {

    weak var output: AlbumInteractorOutput! { didSet { b_output = self.output }}
        
    var favouritesManager: FavouritesManagerProtocol = FavouritesManager()
	var ownStickersManager: OwnStickerManagerProtocol = OwnStickerManager()

	var updatedOwnStickersAlbum: OwnStickerAlbum? {
		ownStickersManager.getOwnStickersAlbum()
	}
	
    func downloadSticker(sticker: StickerProtocol, from album: StickerAlbumProtocol) {
        stickerManager.download(sticker: sticker, from: album) { (error) in
            if let error = error {
                self.output.didReceive(error: error)
            } else {
                self.output.didDownloadSticker(sticker: sticker)
                Reports.logEvent("album_download_sticker_successful", parameters: ["album": album.albumName,
                                                                                   "sticker": sticker.name])
            }
        }
    }
    
    func downloadAlbum(album: StickerAlbumProtocol) {
        stickerManager.download(album: album, prtogressHandler: { (donwlodedSticker) in
            self.output.didDownloadStickerOfAllStickers(sticker: donwlodedSticker)
            Reports.logEvent("album_download_sticker_of_all_successful", parameters: ["album": album.albumName,
                                                                                      "sticker": donwlodedSticker.name])
        }) { (error) in
            if let error = error {
                self.output.didReceive(error: error)
                Reports.logError("album_download_album_error",
                                 error: error,
                                 parameters: ["album": album.albumName])
            } else {
                self.output.didDownloadAllStickers()
                Reports.logEvent("album_download_album", parameters: ["album": album.albumName])
            }
        }

    }
    
    func isStickerDownloaded(sticker: StickerProtocol, from album: StickerAlbumProtocol) -> Bool {
        return stickerManager.isStickerDownloaded(sticker: sticker, from: album)
    }
    
    func stopPlay() {
        stickerManager.stopPlay()
    }
    
    func addFavourites(sticker: StickerProtocol, from album: StickerAlbumProtocol) {
        favouritesManager.addFavourites(sticker: sticker, from: album)
    }

	func deleteSticker(ownSticker: OwnSticker) {
		ownStickersManager.removeSticker(sticker: ownSticker)
	}
}

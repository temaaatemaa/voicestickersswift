//
//  AlbumAlbumInteractorOutput.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovky on 22/05/2019.
//  Copyright © 2019 Zabludovskiy. All rights reserved.
//

import Foundation

protocol AlbumInteractorOutput: BaseInteractorOutputProtocol {
    func didDownloadSticker(sticker: StickerProtocol)
    func didDownloadAllStickers()
    func didDownloadStickerOfAllStickers(sticker: StickerProtocol)
}

//
//  AlbumAlbumModuleInput.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovky on 22/05/2019.
//  Copyright © 2019 Zabludovskiy. All rights reserved.
//

protocol AlbumModuleInput: class {
    func configure(with album: StickerAlbumProtocol)
}

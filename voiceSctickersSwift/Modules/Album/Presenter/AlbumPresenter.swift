//
//  AlbumAlbumPresenter.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovky on 22/05/2019.
//  Copyright © 2019 Zabludovskiy. All rights reserved.
//

class AlbumPresenter: BasePresenter {


    // MARK: VIPER


    weak var view: AlbumViewInput!
    var interactor: AlbumInteractorInput! { didSet { b_interactor = self.interactor } }
    var router: AlbumRouterInput! { didSet { b_router = self.router } }


    // MARK: VARIABLES
    
    
    var vkService: VKServiceProtocol!
    let stickerHelper = StickerHelper()
    
    var album: StickerAlbumProtocol?
    var selectedSticker: StickerProtocol?
}

extension AlbumPresenter: AlbumModuleInput {
    func configure(with album: StickerAlbumProtocol) {
        self.album = album
    }
}

extension AlbumPresenter: AlbumViewOutput {
    
    func viewIsReady() {
        view.update(with: album!)
        view.setupInitialState()
        
        if stickerHelper.isDownloadedAllStickersInAlbum(album: album!) {
            view.hideAlbumDownloadButton()
        } else {
            view.showAlbumDownloadButton()
        }
    }

	func viewWillAppear() {
		if album is OwnStickerAlbum {
			if let newAlbum = interactor.updatedOwnStickersAlbum {
				view.update(with: newAlbum)
			}
		}
	}

    func didTapDownloadAlbum() {
        interactor.downloadAlbum(album: album!)
        Reports.logEvent("album_download_album_button", parameters: ["album": album?.albumName ?? ""])
    }
    
    func didSelectSticker(sticker: StickerProtocol) {
        selectedSticker = sticker
        
        if interactor.isStickerDownloaded(sticker: sticker, from: album!) {
            interactor.play(sticker: sticker, from: album!)
            view.showSendButton()
            Reports.logEvent("album_play", parameters: ["album": album?.albumName ?? "",
                                                        "sticker": sticker.name])
        } else {
            view.showLoadingState(loading: true, for: sticker)
            interactor.downloadSticker(sticker: sticker, from: album!)
            
            Reports.logEvent("album_download_sticker_button", parameters: ["album": album?.albumName ?? "",
                                                                           "sticker": sticker.name])
        }
    }
    
    func didTapSendVk() {
        if vkService.isLogin {
            router.showHud()
            interactor.getFileToSendVk(sticker: selectedSticker!, from: album!) { (file) in
                self.router.hideHud()
                self.router.showChooseVkReceiverModule(with: file)
            }
        } else {
            router.showAlertNeedToLoginVk {
                self.router.showVkLoginModule()
                Reports.logEvent("album_vk_auth", parameters: nil)
            }
        }
    }
    
    func didTapSendTelegram() {
        interactor.getMp3FilePath(sticker: selectedSticker!, from: album!, handler: { (url) in
            self.router.showSendTelegramView(with: url)
        })
    }
    
    func didTapChangeSticker() {
        router.showChangeStickerModule(with: selectedSticker!, from: album!)
        Reports.logEvent("album_change", parameters: ["album": self.album?.albumName ?? "",
                                                      "sticker": selectedSticker!.name])
    }
    
    func isStickerDownloaded(sticker: StickerProtocol) -> Bool {
        return interactor.isStickerDownloaded(sticker: sticker, from: album!)
    }
    
    func viewWillDissapear() {
        interactor.stopPlay()
    }
    
    func didTapFavouritesButton(sticker: StickerProtocol) {
        interactor.addFavourites(sticker: sticker, from: album!)
    }

	func didTapDeleteSticker() {
		if let ownSticker = selectedSticker as? OwnSticker {
			interactor.deleteSticker(ownSticker: ownSticker)
			if let newAlbum = interactor.updatedOwnStickersAlbum {
				view.update(with: newAlbum)
			} else {
				router.closeModule()
			}
		}
	}
}

extension AlbumPresenter: AlbumInteractorOutput {
    func didDownloadStickerOfAllStickers(sticker: StickerProtocol) {
        view.showLoadingState(loading: false, for: sticker)
        view.showDownloadedState(downloaded: true, for: sticker)
    }
    
    func didDownloadSticker(sticker: StickerProtocol) {
        view.showLoadingState(loading: false, for: sticker)
        view.showDownloadedState(downloaded: true, for: sticker)
        interactor.play(sticker: sticker, from: album!)
        view.showSendButton()
    }
    
    func didDownloadAllStickers() {
        view.hideAlbumDownloadButton()
    }
}

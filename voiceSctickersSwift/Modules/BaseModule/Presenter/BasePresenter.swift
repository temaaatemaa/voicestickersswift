//
//  BasePresenter.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 22/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

class BasePresenter {
    var b_interactor: BaseInteractorInputProtocol!
    var b_router: BaseRouterInputProtocol!
}

extension BasePresenter: BaseInteractorOutputProtocol {
    @objc func didBuy() {
        b_router.hideHud()
        b_router.showDidBuyAlert()
    }
    
    @objc func didReceive(error: Error) {
        b_router!.showAlert(for: error)
    }
}

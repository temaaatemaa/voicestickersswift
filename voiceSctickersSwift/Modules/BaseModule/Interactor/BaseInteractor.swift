//
//  BaseInteractor.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 22/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

class BaseInteractor: BaseInteractorInputProtocol {
    
    var b_output: BaseInteractorOutputProtocol!
    var stickerManager: StickerManagerProtocol!
    
    var paymentService: PaymentManagerProtocol!
    
    var isVip: Bool {
        get {
            return paymentService.isBuy
        }
    }
    
    func buyVip() {
        self.paymentService.buy(complitionHandler: { (error) in
            if let error = error {
                Reports.logError("gallary_buy_error", error: error)
                self.b_output.didReceive(error: error)
            } else {
                self.b_output.didBuy()
                Reports.logEvent("gallary_buy", parameters: nil)
            }
        })
    }
	
    func getFileToSendVk(sticker: StickerProtocol, from album: StickerAlbumProtocol, handler: @escaping (Data) -> ()) {
		if let stiker = sticker as? OwnSticker {
			let file = stickerManager.getOwnStickerFile(ownSticker: stiker)
			if let file = file {
				handler(file)
			}
			return
		}
        stickerManager.getData(sticker: sticker, from: album) { (file, error) in
            if let error = error {
                self.b_output.didReceive(error: error)
            } else {
                handler(file!)
                Reports.logEvent("album_vk", parameters: ["album": album.albumName,
                                                          "sticker": sticker.name])
            }
        }
    }
    
    func play(sticker: StickerProtocol, from album: StickerAlbumProtocol) {
        stickerManager.play(sticker: sticker, from: album)
    }
    
    func getMp3FilePath(sticker: StickerProtocol, from album: StickerAlbumProtocol, handler: @escaping (URL) -> ()) {
		if let stiker = sticker as? OwnSticker {
			let url = stickerManager.getOwnStickerFileUrl(ownSticker: stiker)
			if let url = url {
				handler(url)
			}
			return
		}
        stickerManager.getMp3FilePath(sticker: sticker, from: album) { (url) in
            if let url = url {
                handler(url)
            } else {
                let error = NSError(domain: "", code: 2,
                                    userInfo: [NSLocalizedDescriptionKey:"Не удалось! Попробуйте еще раз!"])
                self.b_output.didReceive(error: error)
            }
        }
    }
}

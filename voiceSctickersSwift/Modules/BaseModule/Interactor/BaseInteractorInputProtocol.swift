//
//  BaseInteractorInputProtocol.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 22/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

protocol BaseInteractorInputProtocol {
    var isVip: Bool { get }
    func buyVip()
    
    func getMp3FilePath(sticker: StickerProtocol, from album: StickerAlbumProtocol, handler: @escaping (_ url: URL)->())
    func getFileToSendVk(sticker: StickerProtocol, from album: StickerAlbumProtocol, handler: @escaping (_ file: Data)->())
    func play(sticker: StickerProtocol, from album: StickerAlbumProtocol)
}


//
//  BaseInteractorOutputProtocol.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 22/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

protocol BaseInteractorOutputProtocol: class {
    func didReceive(error: Error)
    func didBuy()
}

//
//  BaseRouterInputProtocol.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 22/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

protocol BaseRouterInputProtocol: class {
    func showAlert(for error: Error)
    func prepare(for segue: UIStoryboardSegue)
    func showHud()
    func hideHud()

	func closeModule()
    
    func showChooseVkReceiverModule(with file: Data)
    func showChangeStickerModule(with sticker: StickerProtocol, from album: StickerAlbumProtocol)
    func showSendTelegramView(with stikerFilePath: URL)
    func showAlertNeedToLoginVk(okButtonAction: @escaping () -> ())
    func showDidBuyAlert()
    func showAlertNeedToBuy(okButtonHandler: @escaping ()->())
    func showVkLoginModule()
}

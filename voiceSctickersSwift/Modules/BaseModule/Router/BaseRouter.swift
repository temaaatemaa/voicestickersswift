//
//  BaseRouter.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 22/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit
import JGProgressHUD

class BaseRouter: BaseRouterInputProtocol {
    var view: UIViewController?
    var hud: JGProgressHUD?
    
    var vkService: VKServiceProtocol!
    var stickerManager: StickerManagerProtocol!
    var telegramSendManager: TelegramSenderManagerProtocol!
    
    func showAlert(for error: Error) {
        AlertHelper.alertFor(error: error, from: view!)
    }
    
    func prepare(for segue: UIStoryboardSegue) {}
    
    func showHud() {
        if hud == nil {
            hud = Hud.loading
        }
        hud?.show(in: view!.view)
    }
    
    func hideHud() {
        hud?.dismiss()
    }

	func closeModule() {
		view?.navigationController?.popViewController(animated: true)
	}
}

extension BaseRouter {
    func showChooseVkReceiverModule(with file: Data) {
        let storyboard = UIStoryboard(name: "ChooseVKStickerReceiver", bundle: nil)
        let chooseVkReceiverModule = storyboard.instantiateInitialViewController() as! ChooseVKReceiverModuleProtocol
        chooseVkReceiverModule.update(with: file)
        view?.present(chooseVkReceiverModule as! UIViewController, animated: true)
    }
    
    func showChangeStickerModule(with sticker: StickerProtocol, from album: StickerAlbumProtocol) {
        let storyboard = UIStoryboard(name: "ChangeAudio", bundle: nil)
        let changeAudioModule = storyboard.instantiateInitialViewController() as! ChangeAudioModuleProtocol
        changeAudioModule.update(with: sticker, from: album)
        view?.present(changeAudioModule as! UIViewController, animated: true)
    }
    
    func showSendTelegramView(with stikerFilePath: URL) {
        telegramSendManager.send(fileUrl: stikerFilePath, from: view!)
    }
    
    func showAlertNeedToLoginVk(okButtonAction: @escaping () -> ()) {
        let alert = AlertHelper.alert(title: "Необходимо авторизоваться в ВК",
                                      message: "Для того, чтобы отправить сообщения в ВК, необходимо войти в свой аккаунт",
                                      leftButtonTitle: "Войти",
                                      rightButtonTitle: "Отмена",
                                      leftButtonHandler: { (_) in
                                        okButtonAction()
        }) { (_) in
            
        }
        view?.present(alert, animated: true)
    }
    
    func showVkLoginModule() {
        vkService.autentification(from: view!)
    }

    func showDidBuyAlert() {
        AlertManager.showDidBuyAlert(from: view!)
    }
    
    func showAlertNeedToBuy(okButtonHandler: @escaping () -> ()) {
        let alert = AlertHelper.alert(title: "Купить VIP статус?",
                                      message: "Чтобы иметь возможность отправлять закрытые альбомы, пожалуйста, купите VIP доступ",
                                      leftButtonTitle: "Купить",
                                      rightButtonTitle: "Отмена",
                                      leftButtonHandler: { (action) in
                                        okButtonHandler()
        }) { (_) in
            Reports.logEvent("buy_cancel", parameters: nil)
        }
        view?.present(alert, animated: true)
    }
}

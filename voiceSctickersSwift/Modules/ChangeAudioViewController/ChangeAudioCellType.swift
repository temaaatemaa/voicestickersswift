//
//  ChangeAudioCellType.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 21/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

enum ChangeAudioCellType {
    case pitchSlider
    case speedSlider
    case playButton
	case saveButton
    case telegramButton
    case vkButton
    
    static func array() -> [ChangeAudioCellType] {
		return [.pitchSlider, .speedSlider, .playButton, .saveButton, .telegramButton, .vkButton]
    }
}


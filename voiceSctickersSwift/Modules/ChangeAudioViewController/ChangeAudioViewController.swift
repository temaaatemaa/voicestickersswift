//
//  ChangeAudioViewController.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 15/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit
import Firebase

protocol ChangeAudioModuleProtocol {
    func update(with sticker: StickerProtocol, from album: StickerAlbumProtocol)
	func update(with ownStickerId: String)
}

class ChangeAudioViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var pitchSlider: UISlider?
    var speedSlider: UISlider?
	var alertTextField: UITextField?
    
    var sticker: StickerProtocol?
    var album: StickerAlbumProtocol?
	var ownStickerId: String?
    var sendingData: Data?
    let cells = ChangeAudioCellType.array()
    
    let stickerManager: StickerManagerProtocol = StickerManager()
	let stickerFileManager: StickerFileManagerProtocol = StickerFileManager()
    let telegramSendManager: TelegramSenderManagerProtocol = TelegramSenderManager()
    let vkService: VKServiceProtocol = VKService()
	let ownStickerManager = OwnStickerManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerCell(cell: SliderTableViewCell.self)
        tableView.registerCell(cell: SettingsTableViewCell.self)
        tableView.register(UINib(nibName: String(describing: SettingsHeaderFooterView.self), bundle: nil),
                           forHeaderFooterViewReuseIdentifier: String(describing: SettingsHeaderFooterView.self))
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toChoose" {
            if let data = sendingData {
                (segue.destination as! ChooseVKStickerReceiverViewController).update(with: data)
            }
        }
    }
    @IBAction func didTapCloseButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        Reports.logEvent("change_close", parameters: ["sticker": sticker?.name ?? "",
                                                        "album": album?.albumName ?? ""])
    }
}

extension ChangeAudioViewController: ChangeAudioModuleProtocol {
    func update(with sticker: StickerProtocol, from album: StickerAlbumProtocol) {
        self.sticker = sticker
        self.album = album
    }

	func update(with ownStickerId: String) {
        self.ownStickerId = ownStickerId
    }
}

extension ChangeAudioViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let type = cells[indexPath.row]
        let cell = cellFor(type: type)
        switch type {
        case .speedSlider, .pitchSlider:
            prepareSliderCell(sliderCell: cell as! SliderTableViewCell, for: type)
		case .playButton, .telegramButton, .vkButton, .saveButton:
            prepareButtonCell(buttonCell: cell as! SettingsTableViewCell, for: type)
        }
        
        return cell
    }
    func cellFor(type: ChangeAudioCellType) -> UITableViewCell {
        var indentifier: String?
        switch type {
        case .pitchSlider, .speedSlider:
            indentifier = SliderTableViewCell.reuseIdentifierString
            break;
        default:
            indentifier = SettingsTableViewCell.reuseIdentifierString
        }
        return self.tableView.dequeueReusableCell(withIdentifier: indentifier!)!
    }
    
    func prepareSliderCell(sliderCell: SliderTableViewCell, for type: ChangeAudioCellType) {
        switch type {
        case .pitchSlider:
            sliderCell.nameLabel.text = "Тембр голоса"
            sliderCell.slider.setValue(1, animated: true)
            sliderCell.slider.minimumValue = 0.1
            sliderCell.slider.maximumValue = 2
            pitchSlider = sliderCell.slider
            break
        case .speedSlider:
            sliderCell.nameLabel.text = "Скорость голоса"
            sliderCell.slider.setValue(1, animated: true)
            sliderCell.slider.minimumValue = 0.6
            sliderCell.slider.maximumValue = 2
            speedSlider = sliderCell.slider
            break
        default:
            break
        }
    }
    
    func prepareButtonCell(buttonCell: SettingsTableViewCell, for type: ChangeAudioCellType) {
        switch type {
        case .playButton:
            buttonCell.nameLabel.text = "Play"
            break
        case.telegramButton:
            buttonCell.nameLabel.text = "Отправить в Телеграм"
            break
        case.vkButton:
            buttonCell.nameLabel.text = "Отправить в ВК"
            break
		case .saveButton:
			buttonCell.nameLabel.text = "Сохранить"
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 123
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: String(describing: SettingsHeaderFooterView.self)) as! SettingsHeaderFooterView
        view.captureLabel?.text = "Изменение"
        view.captureLabel.font = UIFont.systemFont(ofSize: 40, weight: .heavy)
        view.iconImageView.image = UIImage(named: "change-logo")
        return view
    }
}

extension ChangeAudioViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let type = cells[indexPath.row]
        switch type {
        case .pitchSlider, .speedSlider:
            return
        case .playButton:
            play()
            break
        case .vkButton:
            sendVk()
            break
        case .telegramButton:
            sendTelegram()
            break
		case .saveButton:
			save()
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }

	func save() {
		let alertVC = AlertHelper.alertOneButton(title: "Как сохранить?", message: nil, buttonTitle: "OK", buttonAction: { action in
			let text = self.alertTextField?.text
			self.ownStickerManager.save(sticker: self.ownStickerId ?? "", name: text ?? "Неизвестно", changed: true)
			self.dismiss(animated: true, completion: nil)
		})

		alertVC.addTextField { (textField) in
			self.alertTextField = textField
			textField.placeholder = "Название стикера"
		}
		present(alertVC, animated: true, completion: nil)
	}
    
    func play() {
		if let ownStickerId = ownStickerId {
			stickerManager.changeStickerAndPlay(ownSticker: ownStickerId, with: pitchSlider!.value, speed: (speedSlider?.value)!)
		} else {
			if sticker == nil || album == nil {
				return
			}
			stickerManager.changeStickerAndPlay(sticker: sticker!, from: album!, with: pitchSlider!.value, speed: (speedSlider?.value)!)
		}
		Reports.logEvent("change_play", parameters: ["sticker": sticker?.name ?? "", "album": album?.albumName ?? ""])
    }
    
    func sendTelegram() {
		var fileUrl: URL?
		if let ownStickerId = ownStickerId {
			fileUrl = URL(string: stickerFileManager.getChangedRecordedFilePath(for: ownStickerId))
		}  else {
			fileUrl = stickerManager.getChangedMp3FilePath(sticker: sticker!, from: album!)
		}
        if fileUrl == nil {
            return
        }
        telegramSendManager.send(fileUrl: fileUrl!, from: self)
        Reports.logEvent("change_send_telegram", parameters: allParametrs())
    }
    
    func sendVk() {
        if vkService.isLogin {
			if let ownStickerId = ownStickerId {
				let data = stickerFileManager.getChangedRecordedFile(for: ownStickerId)
				self.sendingData = data
				self.performSegue(withIdentifier: "toChoose", sender: self)
				Reports.logEvent("change_send_vk_ok", parameters: self.allParametrs())
				return
			}
            stickerManager.getChangedData(sticker: sticker!, from: album!) { (data, _) in
                if let data = data {
                    self.sendingData = data
                    self.performSegue(withIdentifier: "toChoose", sender: self)
                    Reports.logEvent("change_send_vk_ok", parameters: self.allParametrs())
                } else {
                    AlertManager.showCantChangeSticker(from: self)
                    Reports.logError("change_send_vk_get_channged_data_error", error: nil, parameters: self.allParametrs())
                }
            }
            
            Reports.logEvent("change_send_vk", parameters: allParametrs())
        } else {
            showAlertNeedToLoginVk {
                self.vkService.autentification(from: self)
                Reports.logEvent("change_vk_auth", parameters: nil)
            }
        }
        
    }
    
    func showAlertNeedToLoginVk(okButtonAction: @escaping () -> ()) {
        let alert = AlertHelper.alert(title: "Необходимо авторизоваться в ВК",
                                      message: "Для того, чтобы отправить сообщения в ВК, необходимо войти в свой аккаунт",
                                      leftButtonTitle: "Войти",
                                      rightButtonTitle: "Отмена",
                                      leftButtonHandler: { (_) in
                                        okButtonAction()
        }) { (_) in
            
        }
        self.present(alert, animated: true)
    }

    func allParametrs() -> [String: Any]? {
        return ["sticker": self.sticker?.name ?? "",
                "album": self.album?.albumName ?? "",
                "pitch": self.pitchSlider?.value ?? -100,
                "speed": self.speedSlider?.value ?? -100]
    }
}

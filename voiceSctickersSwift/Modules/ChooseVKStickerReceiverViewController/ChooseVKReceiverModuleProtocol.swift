//
//  ChooseVKReceiverModuleProtocol.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 21/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

protocol ChooseVKReceiverModuleProtocol {
    func update(with sendingData: Data!)
}

//
//  ChooseVKStickerReceiverViewController.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 08/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit
import SDWebImage
import Firebase

class ChooseVKStickerReceiverViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var dialogsButton: UIButton!
    @IBOutlet weak var friendsButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    
    var hud = Hud.loading
    
    var sendingData: Data?
    
    let vkFriendsManager: VKFriendsManagerProtocol = VKFriendsManager()
    let vkDialogManager: VKDialogManagerProtocol = VKDialogManager()
    var vkSendMessageManager: VKSendMessageManagerProtocol = VKSendMessageManager()
    var dialogs: Array<VKDialog>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        vkSendMessageManager.delegate = self
        
        didTapDialogsButton(dialogsButton)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerCell(cell: VKReceiverTableViewCell.self)
        tableView.tableFooterView = UIView()
    }
}


//  MARK: - ChooseVKReceiverModuleProtocol


extension ChooseVKStickerReceiverViewController: ChooseVKReceiverModuleProtocol {
    func update(with sendingData: Data!) {
        self.sendingData = sendingData
    }
}


//  MARK: - TableViewDelegates


extension ChooseVKStickerReceiverViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let member = dialogs![indexPath.row].member!
        if sendingData == nil {
            self.didReceive(error: NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Произошла ошибка: код 228"]))
            return
        }
        vkSendMessageManager.send(file: sendingData!, to: member)
        hud.show(in: view)
        Reports.logEvent("choose_vk_send", parameters: ["member": member.name ?? "",
                                                        "id": member.id ?? "",
                                                        "mess": dialogs![indexPath.row].message ?? "",
                                                        "user": member is VKUser])
    }
}

extension ChooseVKStickerReceiverViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dialogs?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: VKReceiverTableViewCell.reuseIdentifierString) as! VKReceiverTableViewCell
        let dialog = dialogs![indexPath.row]
        cell.messageLabel.text = dialog.message
        cell.nameLable.text = dialog.member?.name
        cell.photoImageView!.sd_setImage(with: URL(string: dialog.member?.photoUrl ?? ""))
        
        return cell
    }
}


// MARK: - VKSendMessageManagerDelegate


extension ChooseVKStickerReceiverViewController: VKSendMessageManagerDelegate {
    func didReceive(error: Error) {
        let alert = AlertHelper.alert(title: "Ошибка", message: error.localizedDescription, leftButtonTitle: "Ok", rightButtonTitle: nil, leftButtonHandler: { (_) in
            self.dismiss(animated: true)
        }) { (_) in
            
        }
        present(alert, animated: true, completion: nil)
        Reports.logError("choose_vk_send_error", error: error)
        hud.dismiss()
    }
    
    func didSendMessage() {
        let alert = AlertHelper.alert(title: "Успешно отправлено!", message: "", leftButtonTitle: "Ok", rightButtonTitle: nil, leftButtonHandler: { (_) in
            self.dismiss(animated: true)
        }) { (_) in
            
        }
        present(alert, animated: true, completion: nil)
        self.tableView.deselectRow(at: tableView.indexPathForSelectedRow!, animated: true)
        self.navigationController?.popViewController(animated: true)
        Reports.logEvent("choose_vk_send_succesful", parameters: nil)
        hud.dismiss()
    }
}

// MARK: - Buttons Actions

extension ChooseVKStickerReceiverViewController {
    @IBAction func didTapFriendsButton(_ sender: Any) {
        changeButtonsUi(clickedButton: sender as! UIButton)
        removeTableViewData()
        
        vkFriendsManager.getFriends { (users, error) in
			guard let users = users else {
				if let error = error {
					AlertHelper.alertFor(error: error, from: self)
				}
				self.hud.dismiss()
				return
			}
            self.vkFriendsManager.getInfo(forUsers: users, complitionHandler: { (users, error) in
                if let error = error {
                    Reports.logError("didTapFriendsButton_error", error: error)
                    self.hud.dismiss()
                    AlertHelper.alertFor(error: error, from: self)
                } else {
                    var dialogs = Array<VKDialog>()
                    for user in users! {
                        dialogs.append(VKDialog.init(message: "", member: user))
                    }
                    self.dialogs = dialogs
                    self.tableView.reloadData()
                    self.hud.dismiss()
                }
            })
        }
    }
    
    @IBAction func didTapDialogsButton(_ sender: Any) {
        changeButtonsUi(clickedButton: sender as! UIButton)
        removeTableViewData()
        
        vkDialogManager.getDialogs(with: 0) { (dialogs, error) in
            self.dialogs = dialogs
            self.tableView.reloadData()
            self.hud.dismiss()
        }
    }
    
    func removeTableViewData() {
        self.dialogs = nil
        self.tableView.reloadData()
        hud.show(in: view)
    }
    
    func changeButtonsUi(clickedButton: UIButton) {
        let unselectedColor = view.backgroundColor
        let selectedColor = UIColor(rgb: 0x1abc9c)
        
        if clickedButton == friendsButton {
            friendsButton.backgroundColor = selectedColor
            dialogsButton.backgroundColor = unselectedColor
        } else if clickedButton == dialogsButton {
            friendsButton.backgroundColor = unselectedColor
            dialogsButton.backgroundColor = selectedColor
        }
    }
    
    @IBAction func didTapCloseButton(_ sender: Any) {
        dismiss(animated: true)
    }
}

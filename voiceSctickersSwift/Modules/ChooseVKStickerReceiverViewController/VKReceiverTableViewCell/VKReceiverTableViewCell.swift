//
//  VKReceiverTableViewCell.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 19/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

class VKReceiverTableViewCell: UITableViewCell {

    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var nameLable: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        photoImageView.clipsToBounds = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        photoImageView.layoutSubviews()
        photoImageView.layer.cornerRadius = photoImageView.frame.width / 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

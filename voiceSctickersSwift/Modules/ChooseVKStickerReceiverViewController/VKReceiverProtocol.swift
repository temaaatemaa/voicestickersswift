//
//  VKReceiverProtocol.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 19/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

protocol VKReceiverProtocol: VKMemberProtocol {
    var name: String? { get set }
    var photoUrl: String? { get set }
}

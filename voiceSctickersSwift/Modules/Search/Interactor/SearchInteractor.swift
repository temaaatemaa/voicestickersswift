//
//  SearchSearchInteractor.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovky on 31/05/2019.
//  Copyright © 2019 Zabludovskiy. All rights reserved.
//

class SearchInteractor: BaseInteractor, SearchInteractorInput {

    weak var output: SearchInteractorOutput! { didSet {b_output = self.output}}

//    var paymentService: PaymentManagerProtocol!
//
//    var isVip: Bool {
//        get {
//            return paymentService.isBuy
//        }
//    }
//
//    func buyVip() {
//        self.paymentService.buy(complitionHandler: { (error) in
//            if let error = error {
//                Reports.logError("gallary_buy_error", error: error)
//                self.output?.didReceive(error: error)
//            } else {
//                self.output?.didBuy()
//                Reports.logEvent("gallary_buy", parameters: nil)
//            }
//        })
//    }
}

//
//  SearchSearchViewOutput.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovky on 31/05/2019.
//  Copyright © 2019 Zabludovskiy. All rights reserved.
//

protocol SearchViewOutput {

    func viewIsReady()
    func searchTextDidChange(text: String)
    func didTapSticker(sticker: SearchStickerProtocol)
    func didTapSendVk()
    func didTapSendTelegram()
    func didTapChangeSticker()
}

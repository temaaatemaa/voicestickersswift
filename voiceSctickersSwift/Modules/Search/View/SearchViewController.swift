//
//  SearchSearchViewController.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovky on 31/05/2019.
//  Copyright © 2019 Zabludovskiy. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {

    var output: SearchViewOutput!
    
    let sendViewTopOffset = 10 as CGFloat
    let sendViewBottomOffset = 10 as CGFloat
    let sendViewHeight = UIScreen.main.bounds.height * 0.1

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    var sendView: SendView?
    
    var stickers = [SearchStickerProtocol]()
    var lockCells: Bool = true
    
    
    // MARK: LIFECYCLE


    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}


// MARK: SearchViewInput


extension SearchViewController: SearchViewInput {

    func setupInitialState() {
        setupTableView()
        setupSearchBar()
    }
    
    func setupTableView() {
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.registerCell(cell: SearchTableViewCell.self)
        
        view.backgroundColor = UIColor(rgb: 0x262429)
        tableView.backgroundColor = UIColor(rgb: 0x262429)
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: sendViewHeight + sendViewBottomOffset + sendViewTopOffset, right: 0)
    }
    
    func setupSearchBar() {
        searchBar.delegate = self
    }
    
    func update(withStickers stickers: [SearchStickerProtocol]) {
        self.stickers = stickers
        self.tableView.reloadData()
    }
    
    func showSendView() {
        if sendView != nil {
            return
        }
        DispatchQueue.main.async { [weak self] in
            self?.sendView = SendView.initFromNib(with: self!)
            self?.sendView?.alpha = 0.02
            self?.view.addSubview((self?.sendView!)!)
            let heightSatus = UIApplication.shared.statusBarFrame.height
            self?.sendView?.frame = CGRect(x: 10,
                                           y: self!.view.frame.height - self!.tabBarController!.tabBar.frame.size.height - self!.sendViewBottomOffset - self!.navigationController!.navigationBar.frame.size.height - heightSatus,
                                           width: self!.view.frame.width - 10 * 2,
                                           height: self!.sendViewHeight)
            UIView.animate(withDuration: 0.5) {
                self?.sendView?.alpha = 1
            }
        }
    }
    func lockCells(lock: Bool) {
        lockCells = lock
        tableView.reloadData()
    }
}


// MARK: TableViewDelegates


extension SearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sticker = stickers[indexPath.item]
        output.didTapSticker(sticker: sticker)
		view.endEditing(true)
    }
}

extension SearchViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stickers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SearchTableViewCell.reuseIdentifierString) as! SearchTableViewCell
        let sticker = stickers[indexPath.item]
        
        cell.albumTitleLabel.text = sticker.albumName
        cell.stickerNameLabel.text = sticker.name
        cell.updateWith(lock: lockCells && sticker.paid)
        
        return cell
    }
}


// MARK: UISearchBarDelegate


extension SearchViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        output.searchTextDidChange(text: searchText)
    }
}


// MARK: - SendViewDelegate


extension SearchViewController: SendViewDelegate {
    func didTapVkButton() {
        output.didTapSendVk()
    }
    
    func didTapTelegramButton() {
        output.didTapSendTelegram()
    }
    
    func didTapChangeButton() {
        output.didTapChangeSticker()
    }
}

//
//  SearchTableViewCell.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 31/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {

    @IBOutlet weak var albumTitleLabel: UILabel!
    @IBOutlet weak var stickerNameLabel: UILabel!
    @IBOutlet weak var separetorView: UIView!
    @IBOutlet weak var separatorViewHeight: NSLayoutConstraint!
    @IBOutlet weak var lockImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.separatorViewHeight.constant = 1 as CGFloat / UIScreen.main.scale
        
        selectedBackgroundView = UIView()
        selectedBackgroundView?.backgroundColor = UIColor.black
        selectedBackgroundView?.layer.borderWidth = 1
        selectedBackgroundView?.layer.borderColor = UIColor(rgb: 0xe74c3c).cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateWith(lock: Bool) {
        lockImageView.isHidden = !lock
    }
    
}

//
//  SearchSearchPresenter.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovky on 31/05/2019.
//  Copyright © 2019 Zabludovskiy. All rights reserved.
//

import Foundation

class SearchPresenter: BasePresenter {


    // MARK: VIPER


    weak var view: SearchViewInput!
    var interactor: SearchInteractorInput! { didSet { b_interactor = self.interactor } }
    var router: SearchRouterInput! { didSet { b_router = self.router } }


    // MARK: VARIABLES
    var vkService: VKServiceProtocol!
    
    var albums: [StickerAlbumProtocol]?
    var allStickers = [SearchStickerProtocol]()
    var stickerHelper = StickerHelper()
    var choosenSticker: SearchStickerProtocol?
    
    override func didBuy() {
        super.didBuy()
        view.lockCells(lock: false)
    }
}

extension SearchPresenter: SearchModuleInput {
    func configure(withAlbums albums: [StickerAlbumProtocol]) {
        self.albums = albums
        for album in albums {
            for sticker in album.stickers {
                let searchSticker = stickerHelper.searchSticker(from: sticker, album: album)
                allStickers.append(searchSticker)
            }
        }
    }
}

extension SearchPresenter: SearchViewOutput {
    func viewIsReady() {
        view.setupInitialState()
        view.lockCells(lock: !interactor.isVip)
        view.update(withStickers: allStickers)
    }
    
    func searchTextDidChange(text: String) {
        if text.count == 0 {
            view.update(withStickers: allStickers)
            return
        }
        
        let predicate = NSPredicate(format: "self Contains[cd] %@", argumentArray: [text])
        
        let filtered = self.allStickers.filter { (sticker) -> Bool in
            return predicate.evaluate(with: sticker.name)
        }
        
        view.update(withStickers: filtered)
    }
    
    func didTapSticker(sticker: SearchStickerProtocol) {
        choosenSticker = sticker
        if checkForBuy(sticker: sticker) == true {
            interactor.play(sticker: sticker, from: albums![0])
            view.showSendView()
        }
    }
    
    func didTapSendVk() {
        if checkForBuy(sticker: choosenSticker!) == false {
            return
        }
        if vkService.isLogin {
            router.showHud()
            interactor.getFileToSendVk(sticker: choosenSticker!, from: albums![0]) { (file) in
                self.router.hideHud()
                self.router.showChooseVkReceiverModule(with: file)
            }
        } else {
            router.showAlertNeedToLoginVk {
                self.router.showVkLoginModule()
                Reports.logEvent("album_vk_auth", parameters: nil)
            }
        }
    }
    
    func didTapSendTelegram() {
        if checkForBuy(sticker: choosenSticker!) == false {
            return
        }
        interactor.getMp3FilePath(sticker: choosenSticker!, from: albums![0], handler: { (url) in
            self.router.showSendTelegramView(with: url)
        })
    }
    
    func didTapChangeSticker() {
        if checkForBuy(sticker: choosenSticker!) == false {
            return
        }
        router.showChangeStickerModule(with: choosenSticker!, from: albums![0])
        Reports.logEvent("search_change", parameters: nil)
    }
    
    func checkForBuy(sticker: SearchStickerProtocol) -> Bool {
        if sticker.paid && !interactor.isVip {
            router.showAlertNeedToBuy {
                self.interactor.buyVip()
            }
            return false
        }
        return true
    }
}

extension SearchPresenter: SearchInteractorOutput {

}

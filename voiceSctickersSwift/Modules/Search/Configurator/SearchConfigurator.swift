//
//  SearchSearchConfigurator.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovky on 31/05/2019.
//  Copyright © 2019 Zabludovskiy. All rights reserved.
//

import UIKit

class SearchModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? SearchViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: SearchViewController) {

        let router = SearchRouter()

        let presenter = SearchPresenter()
        presenter.view = viewController
        presenter.router = router
        presenter.vkService = VKService()

        let interactor = SearchInteractor()
        interactor.output = presenter
        interactor.stickerManager = StickerManager()
        interactor.paymentService = PaymentManager()

        presenter.interactor = interactor
        viewController.output = presenter

        router.view = viewController
        router.vkService = presenter.vkService
        router.stickerManager = interactor.stickerManager
        router.telegramSendManager = TelegramSenderManager()
    }

}

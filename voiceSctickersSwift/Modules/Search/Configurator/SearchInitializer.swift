//
//  SearchSearchInitializer.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovky on 31/05/2019.
//  Copyright © 2019 Zabludovskiy. All rights reserved.
//

import UIKit

class SearchModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var searchViewController: SearchViewController!

    override func awakeFromNib() {

        let configurator = SearchModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: searchViewController)
    }

}

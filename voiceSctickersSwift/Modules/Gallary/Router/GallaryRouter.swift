//
//  GallaryRouter.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 22/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

class GallaryRouter: BaseRouter, GallaryRouterInputProtocol {
    
    var albumToShow: StickerAlbumProtocol?
    
    func showAlbumModule(with album: StickerAlbumProtocol) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "AlbumViewController") as! AlbumViewController
        let albumModule = viewController.output as! AlbumModuleInput
        albumModule.configure(with: album)
        view?.show(viewController, sender: self)
        Reports.logEvent("gallary_go_album", parameters: ["album": album.albumName])
    }
    
    func showSearchModule(with albums: [StickerAlbumProtocol]) {
        let storyboard = UIStoryboard(name: "Search", bundle: nil)
        let viewController = storyboard.instantiateInitialViewController() as! SearchViewController
        let searchModule = viewController.output as! SearchModuleInput
        searchModule.configure(withAlbums: albums)
        view?.show(viewController, sender: self)
        Reports.logEvent("gallary_go_search", parameters: nil)
    }
}

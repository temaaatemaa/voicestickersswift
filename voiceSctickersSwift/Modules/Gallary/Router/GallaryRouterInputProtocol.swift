//
//  GallaryRouterInputProtocol.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 22/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

protocol GallaryRouterInputProtocol: BaseRouterInputProtocol {
    func showAlbumModule(with album: StickerAlbumProtocol)
    func showDidBuyAlert()
    func showSearchModule(with albums: [StickerAlbumProtocol])
}

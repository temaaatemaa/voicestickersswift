//
//  GallaryAssembly.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 22/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

class GallaryConfigurator {
    func configureModuleForViewInput(viewInput: GallaryViewController) {
        let presenter = GallaryPresenter()
        let interactor = GallaryInteractor()
        let router = GallaryRouter()
        
        viewInput.output = presenter
        presenter.view = viewInput
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.output = presenter
        interactor.paymentService = PaymentManager()
        interactor.stickerService = StickersInfoService()
        interactor.favouritesManager = FavouritesManager()
		interactor.ownStickersManager = OwnStickerManager()
        
        router.view = viewInput
    }
}

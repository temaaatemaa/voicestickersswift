//
//  GallaryInitializer.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 23/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

class GallaryInitializer: NSObject {

    @IBOutlet weak var gallaryViewController: GallaryViewController!
    
    override func awakeFromNib() {
        
        let configurator = GallaryConfigurator()
        configurator.configureModuleForViewInput(viewInput: gallaryViewController)
    }
}

//
//  GallaryPresenter.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 22/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

class GallaryPresenter: BasePresenter {
    
    
    // MARK: VIPER
    
    
    weak var view: GallaryViewInputProtocol?
    var interactor: GallaryInteractorInputProtocol? { didSet { b_interactor = self.interactor } }
    var router: GallaryRouterInputProtocol? { didSet { b_router = self.router } }
    
    
    // MARK: VARIABLES
    
    
    var stickerAlbums: [StickerAlbumProtocol]?
    var isTestStickers:Bool = true
    
    
    // MARK: LIFECYCLE
    
    
    deinit {
        NotificationCenter.default.removeObserver(self,
                                                  name: NSNotification.Name(PaymentManagerVipDidBuy),
                                                  object: nil)
    }
    
    override func didBuy() {
        super.didBuy()
        
        view?.updateWith(cells: false)
    }
}


// MARK: GallaryViewOutputProtocol


extension GallaryPresenter: GallaryViewOutputProtocol {
    func viewDidLoad() {
        view?.setupInitialState()
        
        router?.showHud()
        interactor?.requestForStickers()
        
        createDefferedAction()
        
        let vip = interactor?.isVip ?? false
        view?.updateWith(cells: !vip)
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(PaymentManagerVipDidBuy),
                                               object: nil,
                                               queue: OperationQueue.main) { [weak self] (_) in
            self?.didBuy()
        }
    }
    
    func viewDidAppear() {
        addFavouritesAlbumIfNeeded()
		addOwnStickersAlbumIfNeeded()
        view?.updateWith(stickers: stickerAlbums ?? [StickerAlbumProtocol]())
    }
    
    func didChoose(album: StickerAlbumProtocol) {
        if album.paid && interactor!.isVip != true {
            router?.showAlertNeedToBuy(okButtonHandler: { [weak self] in
                self?.router?.showHud()
                self?.interactor!.buyVip()
            })
        } else {
            router?.showAlbumModule(with: album)
        }
    }
    
    func didTapUpdateButton() {
        router?.showHud()
        interactor?.requestForStickers()
    }
    
    func didTapSearchButton() {
        guard let stickerAlbums = stickerAlbums else {
            return
        }
        router?.showSearchModule(with: stickerAlbums)
    }
}


// MARK: GallaryInteractorOutputProtocol


extension GallaryPresenter: GallaryInteractorOutputProtocol {
    
    override func didReceive(error: Error) {
        router?.hideHud()
        router?.showAlert(for: error)
    }
    
    func didReceive(stickers: [StickerAlbumProtocol], isTestVersion: Bool) {
        view?.showUpdateButton(show: false)
        router?.hideHud()
        stickerAlbums = stickers
        addFavouritesAlbumIfNeeded()
		addOwnStickersAlbumIfNeeded()
        view?.updateWith(stickers: stickerAlbums!)
    }
    
    func addFavouritesAlbumIfNeeded() {
        let album = interactor?.getFavouritesAlbum()
        if stickerAlbums?[0] is FavouriteAlbum {
            stickerAlbums?.remove(at: 0)
        }
        if let album = album {
            stickerAlbums?.insert(album, at: 0)
        }
    }

	func addOwnStickersAlbumIfNeeded() {
        let album = interactor?.getOwnStickersAlbum()
        if stickerAlbums?[0] is FavouriteAlbum {
			if stickerAlbums?[1] is OwnStickerAlbum {
				stickerAlbums?.remove(at: 1)

			}
			if let album = album {
				stickerAlbums?.insert(album, at: 1)
			}
			return
		} else {
			if stickerAlbums?[0] is OwnStickerAlbum {
				stickerAlbums?.remove(at: 0)

			}
			if let album = album {
				stickerAlbums?.insert(album, at: 0)
			}
			return
		}

    }
}


// MARK: Private Methods


extension GallaryPresenter {
    func createDefferedAction() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) { [weak self] in
            if self?.stickerAlbums == nil {
                self?.router?.hideHud()
                self?.view?.showUpdateButton(show: true)
            }
        }
    }
}

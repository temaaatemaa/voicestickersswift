//
//  GallaryInteractor.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 22/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

class GallaryInteractor: BaseInteractor, GallaryInteractorInputProtocol {
    
    weak var output: GallaryInteractorOutputProtocol! {didSet {b_output = self.output}}
    var stickerService: StickersInfoServiceProtocol!
    var favouritesManager: FavouritesManagerProtocol!
	var ownStickersManager: OwnStickerManagerProtocol!
    
    func requestForStickers() {
        stickerService.getStickers { [weak self] (stickers, isTest, error) in
            if let error = error {
                Reports.logError("gallary_get_stickers_error", error: error)
                self?.output?.didReceive(error: error)
            } else if stickers == nil {
                Reports.logEvent("gallary_no_stickers", parameters: nil)
            } else {
                if stickers?.count ?? 0 < 3 {
                    Reports.logError("getStickers_stickers_min_3", error: nil, parameters: nil)
                }
                self?.output?.didReceive(stickers: stickers!, isTestVersion: isTest)
                Reports.logEvent("gallary_get_stickers", parameters: ["test": isTest,
                                                                      "count": stickers?.count ?? 0])
            }
        }
    }
    
    func getFavouritesAlbum() -> StickerAlbumProtocol? {
        return favouritesManager.getFavouritesAlbum()
    }

	func getOwnStickersAlbum() -> StickerAlbumProtocol? {
		return ownStickersManager.getOwnStickersAlbum()
	}
}

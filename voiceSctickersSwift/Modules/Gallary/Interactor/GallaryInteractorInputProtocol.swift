//
//  GallaryInteractorInputProtocol.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 22/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

protocol GallaryInteractorInputProtocol: BaseInteractorInputProtocol {
    var isVip: Bool { get }
    func buyVip()
    
    func requestForStickers()
    
    func getFavouritesAlbum() -> StickerAlbumProtocol?
	func getOwnStickersAlbum() -> StickerAlbumProtocol?
}

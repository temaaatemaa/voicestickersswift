//
//  GallaryViewInputProtocol.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 22/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

protocol GallaryViewInputProtocol: class {
    func setupInitialState()
    func updateWith(stickers: [StickerAlbumProtocol])
    func updateWith(cells isLock: Bool)
    func showUpdateButton(show: Bool)
}

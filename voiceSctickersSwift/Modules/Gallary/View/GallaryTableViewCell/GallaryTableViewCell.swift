//
//  GallaryTableViewCell.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 16/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit
import SnapKit

class GallaryTableViewCell: UITableViewCell {

    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var nameLable: UILabel!
    
    var lockView: LockView? {
        didSet {
            self.contentView.addSubview(self.lockView!)
            lockView?.snp.makeConstraints({ (make) in
                make.center.equalTo(self.contentView)
                make.height.width.equalTo(self.contentView)
            })
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    public func setLock(lock: Bool) {
        if lock {
            if self.lockView == nil {
                self.lockView = LockView.initFromNib()
            }
            self.lockView?.isHidden = false
        } else {
            self.lockView?.isHidden = true
        }
    }
}

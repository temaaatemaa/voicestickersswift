//
//  LockView.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 16/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

class LockView: UIView {

    static func initFromNib() -> LockView {
        return UINib(nibName: String(describing: LockView.self), bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! LockView
    }
}

//
//  GallaryViewOutputProtocol.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 22/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

protocol GallaryViewOutputProtocol {
    func viewDidLoad()
    func viewDidAppear()
    func didChoose(album: StickerAlbumProtocol)
    func didTapUpdateButton()
    func didTapSearchButton()
}

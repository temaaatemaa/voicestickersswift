//
//  ViewController.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 07/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit
import SDWebImage

class GallaryViewController: UIViewController  {
    
    var output: GallaryViewOutputProtocol!

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchButton: UIButton!
    
    var stickerAlbums: Array<StickerAlbumProtocol>?
    var lockCells: Bool?
    var showUpdateCell: Bool!
    
    let cahedManager = ImageCacheManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        output?.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        output.viewDidAppear()
//        showDisclaimerManager.showDisclaymerIfNeeded(on: self)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setupTableView() {
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self

        tableView.registerCell(cell: UpdateTableViewCell.self)
        tableView.registerCell(cell: GallaryTableViewCell.self)
        
        view.backgroundColor = UIColor(rgb: 0x262429)
        tableView.backgroundColor = UIColor(rgb: 0x262429)
    }
    
    @IBAction func didTapSearchButton(_ sender: Any) {
        output.didTapSearchButton()
    }
}

extension GallaryViewController: GallaryViewInputProtocol {
    
    func setupInitialState() {
        setupTableView()
        showUpdateCell = false
    }
    
    func showUpdateButton(show: Bool) {
        showUpdateCell = show
        tableView.reloadData()
    }
    
    func updateWith(stickers: [StickerAlbumProtocol]) {
        stickerAlbums = stickers
        tableView.reloadData()
    }
    
    func updateWith(cells isLock: Bool) {
        lockCells = isLock
        tableView.reloadData()
    }
}

extension GallaryViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  showUpdateCell ? 1 : stickerAlbums?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if showUpdateCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: UpdateTableViewCell.reuseIdentifierString) as! UpdateTableViewCell
            cell.delegate = self
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: GallaryTableViewCell.reuseIdentifierString) as! GallaryTableViewCell
        
        let album = stickerAlbums![indexPath.row] as StickerAlbumProtocol
        
        cell.nameLable.text = album.albumName
        if lockCells ?? true {
            cell.setLock(lock: album.paid)
        } else {
            cell.setLock(lock: false)
        }
        let previewUrl = album.albumPreviewUrl
        let placeHolder = UIImage(named: "placeholder")
        let cachedImage = cahedManager.getImage(for: previewUrl)
        if cachedImage == nil {
            cell.previewImageView.sd_setImage(with: URL(string: previewUrl), placeholderImage: placeHolder, options: .highPriority) { (image, error, cache, url) in
                self.cahedManager.saveImage(image: image, error: error, cache: cache, url: url)
            }
        } else {
            cell.previewImageView.image = cachedImage
        }
        
        return cell
    }
}

extension GallaryViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if stickerAlbums?.count ?? 0 > 0 {
            let album = stickerAlbums![indexPath.row]
            output?.didChoose(album: album)
        }
    }
}

extension GallaryViewController: UpdateTableViewCellDelegate {
    func didTapUpdateButton() {
        output.didTapUpdateButton()
    }
}

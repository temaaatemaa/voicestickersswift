//
//  UpdateTableViewCell.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 24/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

protocol UpdateTableViewCellDelegate {
    func didTapUpdateButton()
}

class UpdateTableViewCell: UITableViewCell {
    
    var delegate: UpdateTableViewCellDelegate?
    
    @IBAction func didTapUpdateButton(_ sender: Any) {
        delegate?.didTapUpdateButton()
    }
}

//
//  RecordViewController.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 11.04.2020.
//  Copyright © 2020 n. All rights reserved.
//

import UIKit
import AVFoundation
import SnapKit

class RecordViewController: UIViewController {

    let telegramSendManager: TelegramSenderManagerProtocol = TelegramSenderManager()
	let ownStickerManager = OwnStickerManager()
	let fileManager = StickerFileManager()
	var recordingManager: RecorderManager?
	var recordingStatus = false
	var sendingData: Data?
	let player = StickerPlayerManager()
	let vkService: VKServiceProtocol = VKService()

	var sendView: SendView?
	var siriContainer: UIView = UIView()
	var recordButton: UIButton = UIButton()
	var siriView: SiriWaveView = SiriWaveView(frame: .zero)
	var playButton: UIButton = UIButton()
	var saveButton: UIButton = UIButton()

	var screenHeight: CGFloat {
		return UIScreen.main.bounds.height
	}

	lazy var currentId: String = ownStickerManager.newId

	override var preferredStatusBarStyle: UIStatusBarStyle {
		.lightContent
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()

		setupSiriView()
		setupRecordButton()
		setupPlayButton()
		setupSaveButton()


		setupRecordingForNewFile()
//
//		dialogue.rate = AVSpeechUtteranceDefaultSpeechRate;
//        dialogue.voice = AVSpeechSynthesisVoice(language: "ru-ru")
//
//        guard speaker.isSpeaking else
//        {
//            speaker.speak(dialogue)
//            return
//        }
	}

//	let speaker = AVSpeechSynthesizer()
//    let dialogue = AVSpeechUtterance(string: "Привет!НИКНУБ НИКНУБНИКНУБ")

	override func viewWillAppear(_ animated: Bool) {
		setNeedsStatusBarAppearanceUpdate()
	}

	func setupRecordingForNewFile() {
		DispatchQueue.global().async {
			let audioFilename = self.fileManager.getRecordedFilePath(for: self.currentId)
			let url = URL(string: audioFilename)!
			self.recordingManager = RecorderManager(to: url)
			self.recordingManager?.delegate = self
			self.player.delegate = self
			do {
				try self.recordingManager?.prepare()
			} catch {
				//return alert
			}
		}
	}

	func setupSaveButton() {
		saveButton.setImage(#imageLiteral(resourceName: "save"), for: .normal)
		saveButton.addTarget(self, action: #selector(didTapSaveButton), for: .touchUpInside)
		view.addSubview(saveButton)
		saveButton.alpha = 0.02
		saveButton.snp.makeConstraints { (make) in
			make.centerY.equalTo(recordButton)
			make.left.equalToSuperview().offset(screenHeight * 0.05)
			make.height.width.equalTo(recordButton)
		}
	}

	var alertTextField: UITextField?
	@objc func didTapSaveButton() {
		let alertVC = AlertHelper.alertOneButton(title: "Как сохранить?", message: nil, buttonTitle: "OK", buttonAction: { action in
			let text = self.alertTextField?.text
			self.ownStickerManager.save(sticker: self.currentId, name: text ?? "Неизвестно", changed: false)
			self.currentId = self.ownStickerManager.newId
			self.setupRecordingForNewFile()
			self.hideButtons()
		})

		alertVC.addTextField { (textField) in
			self.alertTextField = textField
			textField.placeholder = "Название стикера"
		}
		present(alertVC, animated: true, completion: nil)
	}

	func setupPlayButton() {
		playButton.setImage(#imageLiteral(resourceName: "play"), for: .normal)
		playButton.addTarget(self, action: #selector(didTapPlayButton(_:)), for: .touchUpInside)
		view.addSubview(playButton)
		playButton.alpha = 0.02
		playButton.snp.makeConstraints { (make) in
			make.centerY.equalTo(recordButton)
			make.right.equalToSuperview().offset(-screenHeight * 0.05)
			make.height.width.equalTo(recordButton)
		}
	}

	func setupRecordButton() {
		let inset = screenHeight * 0.1 / 4
		recordButton.imageEdgeInsets = UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)
		recordButton.backgroundColor = .red
		recordButton.layer.cornerRadius = screenHeight * 0.1 / 2
		recordButton.addTarget(self, action: #selector(didTapRecordButton(_:)), for: .touchUpInside)
		view.addSubview(recordButton)
		recordButton.snp.makeConstraints { (make) in
			make.bottom.equalToSuperview().offset(-screenHeight * 0.05)
			make.centerX.equalToSuperview()
			make.height.width.equalTo(screenHeight * 0.1)
		}
	}

	func setupSiriView() {
		siriContainer.backgroundColor = #colorLiteral(red: 0.1706914306, green: 0.1706914306, blue: 0.1706914306, alpha: 1)
		siriContainer.layer.cornerRadius = 10
		view.addSubview(siriContainer)
		siriContainer.snp.makeConstraints { (make) in
			make.top.equalToSuperview().offset(screenHeight * 0.05)
			make.left.equalToSuperview().offset(10)
			make.right.equalToSuperview().inset(10)
			make.height.equalTo(screenHeight * 0.25)
		}

		siriView.backgroundColor = .clear
		siriContainer.addSubview(siriView)
		siriView.snp.makeConstraints { (make) in
			make.left.right.equalToSuperview()
			make.top.equalToSuperview().offset(5)
			make.bottom.equalToSuperview().offset(-5)
		}
	}

	@objc func didTapRecordButton(_ sender: Any) {
		siriView.commonInit()
		recordingStatus.toggle()
		if recordingStatus == true {
			recordButton.setImage(UIImage(named: "stop"), for: .normal)
			try? recordingManager?.record()
			hideButtons()
		} else {
			siriView.update(0)
			recordButton.setImage(nil, for: .normal)
			recordingManager?.stop()
			playButton.isHidden = false
			showButtons()
		}
	}

	@objc func didTapPlayButton(_ sender: Any) {
		if let file = fileManager.getRecordedFile(for: currentId) {
			player.play(file: file)
		}
	}

	func showButtons() {
		showSendButton()

		self.playButton.isHidden = false
		self.saveButton.isHidden = false

		UIView.animate(withDuration: 0.5) {
			self.playButton.alpha = 1
			self.saveButton.alpha = 1
		}
	}

	func hideButtons() {
		removeSendButton()
		siriView.update(0)
		UIView.animate(withDuration: 0.5) {

		}

		UIView.animate(withDuration: 0.5, animations: {
			self.playButton.alpha = 0.02
			self.saveButton.alpha = 0.02
		}) { (success) in
			self.playButton.isHidden = true
			self.saveButton.isHidden = true
		}
	}


	func showSendButton() {
		if sendView != nil {
			return
		}
		self.sendView = SendView.initFromNib(with: self)
		self.sendView?.alpha = 0.02
		self.sendView?.backgroundColor = #colorLiteral(red: 0.1159973219, green: 0.1159973219, blue: 0.1159973219, alpha: 1)
		self.view.addSubview(self.sendView!)
		self.sendView?.snp.makeConstraints({ (maker) in
			maker.bottom.equalTo(self.recordButton.snp_topMargin).offset(-UIScreen.main.bounds.height * 0.03)
			maker.left.equalToSuperview().offset(10)
			maker.right.equalToSuperview().inset(10)
			maker.height.equalTo(UIScreen.main.bounds.height * 0.1)
		})
		UIView.animate(withDuration: 0.5) {
			self.sendView?.alpha = 1
		}
	}

	func removeSendButton() {
		if sendView == nil {
			return
		}
		UIView.animate(withDuration: 0.5, animations: {
			self.sendView?.alpha = 0.02
		}) { (success) in
			self.sendView?.removeFromSuperview()
			self.sendView = nil
		}
	}

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "toChoose" {
			if let data = sendingData {
				(segue.destination as! ChooseVKStickerReceiverViewController).update(with: data)
			}
		}
	}
}

extension RecordViewController: RecorderManagerDelegate {
	func audioMeterDidUpdate(_ dB: Float) {
		let value = normalizedPowerLevelFromDecibels(decibels: dB)
		siriView.update(CGFloat(value) * 10)
	}

	private func normalizedPowerLevelFromDecibels(decibels: Float) -> Float {
		let minDecibels: Float = -60.0
		if (decibels < minDecibels || decibels.isZero) {
			return .zero
		}

		let powDecibels = pow(10.0, 0.05 * decibels)
		let powMinDecibels = pow(10.0, 0.05 * minDecibels)
		return pow((powDecibels - powMinDecibels) * (1.0 / (1.0 - powMinDecibels)), 1.0 / 2.0)
	}
}


extension RecordViewController: SendViewDelegate {
	func didTapVkButton() {
		if vkService.isLogin {
			self.sendingData = fileManager.getRecordedFile(for: currentId)
			self.performSegue(withIdentifier: "toChoose", sender: self)
		}
		else {
			showAlertNeedToLoginVk {
				self.vkService.autentification(from: self)
				Reports.logEvent("change_vk_auth", parameters: nil)
			}
		}
	}

	func showAlertNeedToLoginVk(okButtonAction: @escaping () -> ()) {
		let alert = AlertHelper.alert(title: "Необходимо авторизоваться в ВК",
									  message: "Для того, чтобы отправить сообщения в ВК, необходимо войти в свой аккаунт",
									  leftButtonTitle: "Войти",
									  rightButtonTitle: "Отмена",
									  leftButtonHandler: { (_) in
										okButtonAction()
		}) { (_) in

		}
		self.present(alert, animated: true)
	}

	func didTapTelegramButton() {
		let fileUrl = URL(string: fileManager.getChangedRecordedFilePath(for: currentId))

		if fileUrl == nil {
            return
        }
        telegramSendManager.send(fileUrl: fileUrl!, from: self)
	}

	func didTapChangeButton() {
		let storyboard = UIStoryboard(name: "ChangeAudio", bundle: nil)
        let changeAudioModule = storyboard.instantiateInitialViewController() as! ChangeAudioModuleProtocol
        changeAudioModule.update(with: currentId)
		present(changeAudioModule as! UIViewController, animated: true, completion: nil)
	}
}

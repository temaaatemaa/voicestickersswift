//
//  StickerAlbum.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 07/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

protocol StickerAlbumProtocol {
    var albumName: String { get }
    var albumPreviewUrl: String { get }
    var paid: Bool { get }
    var stickers: Array<StickerProtocol> { get }
}

struct StickerAlbum : StickerAlbumProtocol {
    var albumName: String
    var albumNumber: Int
    var albumPreviewUrl: String
    var paid: Bool
    var stickers: Array<StickerProtocol>
}

protocol TestStickerAlbumProtocol {
}

struct TestStickerAlbum : StickerAlbumProtocol, TestStickerAlbumProtocol {
    var albumName: String
    var albumPreviewUrl: String
    var paid: Bool
    var stickers: Array<StickerProtocol>
}

//
//  Sticker.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 07/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

protocol StickerProtocol {
    var name: String { get }
}

struct Sticker: StickerProtocol {
    var name: String
}

protocol TestStickerProtocol {
    var albumNumber: Int { get }
    var soundNumber: Int { get }
}

struct TestSticker: StickerProtocol, TestStickerProtocol {
    var name: String
    var albumNumber: Int
    var soundNumber: Int
}

protocol SearchStickerProtocol: StickerProtocol, TestStickerProtocol {
    var albumName: String { get }
    var paid: Bool { get }
}

struct SearchSticker: SearchStickerProtocol {
    var name: String
    var albumNumber: Int
    var soundNumber: Int
    var albumName: String
    var paid: Bool
}

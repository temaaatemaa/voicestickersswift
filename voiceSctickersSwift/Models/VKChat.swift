//
//  VKChat.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 27/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

struct VKChat: VKMemberProtocol, VKReceiverProtocol {
    var id: Int?
    var name: String?
    var photoUrl: String?
}

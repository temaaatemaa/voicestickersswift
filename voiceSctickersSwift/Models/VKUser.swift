//
//  VKUser.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 08/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

struct VKUser: VKMemberProtocol, VKReceiverProtocol {
    var firstName: String?
    var lastName: String?
    var online: Bool = false
    var photoUrl: String?
    var id: Int?
    var name: String? { get { return (firstName ?? "") + " " + (lastName ?? "") } set {} }
}

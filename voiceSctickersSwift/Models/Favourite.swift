//
//  Favourite.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 28/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

struct FavouriteSticker: Codable, StickerProtocol, TestStickerProtocol {
    var albumNumber: Int
    var soundNumber: Int
    var name: String
}

struct FavouriteAlbum: StickerAlbumProtocol, TestStickerAlbumProtocol {
    var albumName: String
    var albumPreviewUrl: String
    var paid: Bool
    var stickers: Array<StickerProtocol>
}

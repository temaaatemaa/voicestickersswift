//
//  VKMemberProtocol.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 08/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

protocol VKMemberProtocol {
    var id: Int? { get set }
}

//
//  VKGroup.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 08/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

struct VKGroup: VKMemberProtocol, VKReceiverProtocol {    
    var id: Int?
    var name: String?
    var photoUrl: String?
}

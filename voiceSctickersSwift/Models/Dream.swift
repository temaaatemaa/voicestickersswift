//
//  Dream.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 20/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

class Dream: Codable {

    var id: String?
    var name: String?
    var votedCount: Int?
    
    init(id: String?, name: String?, votedCount: Int?) {
        self.id = id
        self.name = name
        self.votedCount = votedCount
    }
}

//
//  VKDialog.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 08/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

struct VKDialog {
    var message: String?
    var member: VKReceiverProtocol?
}

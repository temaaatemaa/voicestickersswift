//
//  OwnSticker.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 12.04.2020.
//  Copyright © 2020 n. All rights reserved.
//

import UIKit

struct OwnSticker: Codable, StickerProtocol {
	var id: String
    var name: String
	var changed: Bool
}

struct OwnStickerAlbum: StickerAlbumProtocol {
    var albumName: String
    var albumPreviewUrl: String
    var paid: Bool
    var stickers: Array<StickerProtocol>
}

//
//  TableView+RegisterCell.swift
//  ArtsApp
//
//  Created by Artem Zabludovsky on 24/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

extension UITableView {
    func registerCell<Subject>(cell: Subject) {
        self.register(UINib(nibName: String(describing: cell), bundle: nil),
                      forCellReuseIdentifier: String(describing: cell))
    }
}

//
//  TableViewCell+ReuseIndentifier.swift
//  ArtsApp
//
//  Created by Artem Zabludovsky on 24/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

extension UITableViewCell {
    class var reuseIdentifierString: String! {
        get {
            let id = String(describing: self.self)
            return id
        }
    }
}

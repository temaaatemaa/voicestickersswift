//
//  TelegramSenderManager.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 21/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

class TelegramSenderManager: TelegramSenderManagerProtocol {
    
    func send(fileUrl: URL, from viewController: UIViewController) {
        let activityVC = UIActivityViewController.init(activityItems: [fileUrl], applicationActivities: nil)
        activityVC.excludedActivityTypes = [UIActivity.ActivityType.mail, UIActivity.ActivityType.airDrop, UIActivity.ActivityType.message, UIActivity.ActivityType.copyToPasteboard]
        activityVC.popoverPresentationController?.sourceView = viewController.view
        viewController.present(activityVC, animated: true)
    }
    
}

//
//  PaymentManagerProtocol.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 16/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

protocol PaymentManagerProtocol {
    var isBuy: Bool { get }
    func buy(complitionHandler: @escaping (_ error: Error?) -> ())
    func restore(complitionHandler: @escaping (_ error: Error?) -> ())
}

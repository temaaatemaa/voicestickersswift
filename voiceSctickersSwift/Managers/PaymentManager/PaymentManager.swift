//
//  PaymentManager.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 16/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit
import IAPurchaseManager

let PaymentManagerVipKey = "buyAllStickers"
let PaymentManagerVipDidBuy = "vip.purchase"

class PaymentManager: PaymentManagerProtocol {
    
    let vipIndentifier = "n.vkVoiceStickers.vip"
    
    var isBuy: Bool {
        get {
            return UserDefaults.standard.bool(forKey: PaymentManagerVipKey)
        }
    }
    
    func buy(complitionHandler: @escaping (Error?) -> ()) {
        IAPManager.shared.purchaseProductWithId(productId: vipIndentifier) { (error) in
            if error == nil {
                UserDefaults.standard.set(true, forKey: PaymentManagerVipKey)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: PaymentManagerVipDidBuy), object: nil)
                Reports.logEvent("PaymentManager_buy", parameters: nil)
            }
            complitionHandler(error)
        }
    }
    
    func restore(complitionHandler: @escaping (Error?) -> ()) {
        IAPManager.shared.restoreCompletedTransactions { (error) in
            if error == nil {
                UserDefaults.standard.set(true, forKey: PaymentManagerVipKey)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: PaymentManagerVipDidBuy), object: nil)
                Reports.logEvent("PaymentManager_resore", parameters: nil)
            }
            complitionHandler(error)
        }
    }
    
    
}

//
//  AudioRecordManager.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 11.04.2020.
//  Copyright © 2020 n. All rights reserved.
//

import UIKit
import AVFoundation

class AudioRecordManager: NSObject {
	var audioRecorder: AVAudioRecorder?

	func recordAudio(on url: URL) {

		let settings = [
			AVFormatIDKey: Int(kAudioFormatFLAC),
			AVSampleRateKey: 12000,
			AVNumberOfChannelsKey: 1,
			AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
		]

		do {
			audioRecorder = try AVAudioRecorder(url: url, settings: settings)
			audioRecorder?.delegate = self
			audioRecorder?.record()
		} catch {
			print(error)
		}
	}

	func stopRecording() {
		audioRecorder?.stop()
	}
}

extension AudioRecordManager: AVAudioRecorderDelegate {
	
}

//
//  RecorderManager.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 11.04.2020.
//  Copyright © 2020 n. All rights reserved.
//

import AVFoundation
import QuartzCore

public protocol RecorderManagerDelegate: AVAudioRecorderDelegate {
	func audioMeterDidUpdate(_ dB: Float)
}

public class RecorderManager : NSObject {

	@objc public enum State: Int {
		case None, Record
	}

	public weak var delegate: RecorderManagerDelegate?
	public private(set) var url: URL
	public private(set) var state: State = .None

	public var metering: Bool = true

	private let session = AVAudioSession.sharedInstance()
	private var recorder: AVAudioRecorder?
	private var player: AVAudioPlayer?
	private var link: CADisplayLink?



	// MARK: - Initializers
	public init(to: URL) {
		url = to
		super.init()
	}

	// MARK: - Record
	public func prepare() throws {
		let settings = [
			AVFormatIDKey: Int(kAudioFormatFLAC),
			AVSampleRateKey: 12000,
			AVNumberOfChannelsKey: 1,
			AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
		]

		recorder = try AVAudioRecorder(url: url, settings: settings)
		recorder?.prepareToRecord()
		recorder?.delegate = delegate
		recorder?.isMeteringEnabled = metering
	}

	public func record() throws {
		if recorder == nil {
			try prepare()
		}

		try session.setCategory(.playAndRecord)

		recorder?.record()
		state = .Record

		if metering {
			startMetering()
		}
	}

	public func stop() {
	  switch state {
	  case .Record:
		recorder?.stop()
		recorder = nil
		stopMetering()
	  default:
		break
	  }

	  state = .None
	}


	// MARK: - Metering
	@objc func updateMeter() {
		guard let recorder = recorder else { return }

		recorder.updateMeters()

		let dB = recorder.averagePower(forChannel: 0)

		delegate?.audioMeterDidUpdate(dB)
	}

	private func startMetering() {
		link = CADisplayLink(target: self, selector: #selector(updateMeter))
		link?.add(to: RunLoop.current, forMode: RunLoop.Mode.common)
	}

	private func stopMetering() {
		link?.remove(from: RunLoop.current, forMode: RunLoop.Mode.common)
		link = nil
	}
}

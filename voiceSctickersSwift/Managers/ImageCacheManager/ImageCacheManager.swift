//
//  ImageCacheManager.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 27/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit
import SDWebImage

class ImageCacheManager: NSObject {
    let fileManager = FileManager.default
    var documentDirectory: URL? {
        get {
            return try? fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:false)
        }
    }
    
    func getImage(for url: String) -> UIImage? {

        let image = UIImage.init(contentsOfFile: (documentDirectory?.appendingPathComponent(getFileNameFromUrl(url: url)).path)!)
        return image
    }
    
    func saveImage(image: UIImage?, error: Error?, cache: SDImageCacheType, url: URL?) {
        if let image = image {
            guard let data = image.jpegData(compressionQuality: 1) else { return }
            try? data.write(to: (documentDirectory?.appendingPathComponent(getFileNameFromUrl(url: url?.absoluteString ?? "")))!)
        }
    }
    
    func getFileNameFromUrl(url: String) -> String {
        return url
    }
}

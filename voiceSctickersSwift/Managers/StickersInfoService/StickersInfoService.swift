//
//  StickersService.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 07/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit
import Firebase

typealias GetStickersComplitionHandler = ((_ stickers: Array<StickerAlbumProtocol>?, _ test: Bool, _ error: Error?) -> ())

class StickersInfoService: StickersInfoServiceProtocol {
    
    let stickerParser = StickerParser()
    let applicationHelper = ApplicationHelper()
    
    var getStickersComplitionHandler: GetStickersComplitionHandler?
    
    var stickersKey: String {
        get {
            let versionWithDot = applicationHelper.getApplicationVersion()
            let versionWithoutDot = versionWithDot.replacingOccurrences(of: ".", with: "_")
            return "sticker_version_\(versionWithoutDot)"
        }
    }
    
    var arrayKey: String {
        get {
            return applicationHelper.getApplicationVersion() + "-array"
        }
    }
    var listKey: String {
        get {
            return applicationHelper.getApplicationVersion() + "-list"
        }
    }
    
    init() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(didChangeRemoteConfig), name: Notification.Name(FirebaseRemoteConfigDidChangeNotification), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name(FirebaseRemoteConfigDidChangeNotification), object: nil)
    }
    
    @objc fileprivate func didChangeRemoteConfig() {
        if let complitionHandler = getStickersComplitionHandler {
            let key = "test_mode_\(applicationHelper.getApplicationVersion().replacingOccurrences(of: ".", with: "_"))"
            let isTest = RemoteConfig.remoteConfig()[key].boolValue
            getStickers(testVersion: isTest, complitionHandler: complitionHandler)
        }
    }
    
    func getStickers(complitionHandler: @escaping GetStickersComplitionHandler) {
        getStickersComplitionHandler = complitionHandler
        getStickers(testVersion: getArrayFromUserDef() == nil, complitionHandler: complitionHandler)
    }
    
    func getStickers(testVersion: Bool, complitionHandler: @escaping GetStickersComplitionHandler) {
        if testVersion {
            downloadTestArray(comlitionHandler: complitionHandler)
        } else {
            getNormalStickers(complitionHandler: complitionHandler)
        }
    }

    public func getNormalStickers(complitionHandler: @escaping GetStickersComplitionHandler) {
        var stickersArray: Array<StickerAlbum>?
        var stickerList: Array<String>?
        
        (stickersArray, stickerList) = getStickersFromUDIfNeeded()
        
        let group = DispatchGroup.init()
        var reqError: Error?
        
        if stickerList == nil || stickersArray == nil {
            self.downloadArray {stickers, error in
                if let error = error {
                    complitionHandler(nil, false, error)
                } else {
                    stickersArray = stickers
                    self.downloadList {list, error in
                        if let error = error {
                            complitionHandler(nil, false, error)
                        } else {
                            stickerList = list
                            UserDefaults.standard.set(RemoteConfig.remoteConfig()[self.stickersKey].stringValue, forKey: self.stickersKey)
                            let orderArray = self.orderArray(array: stickersArray!, order: stickerList!)
                            complitionHandler(orderArray, false, nil)
                        }
                    }
                }
            }
        } else {
            let orderArray = self.orderArray(array: stickersArray!, order: stickerList!)
            complitionHandler(orderArray, false, nil)
        }
        
        
//        DispatchQueue.global().async {
//            group.enter()
//            if stickersArray != nil {
//                group.leave()
//                return
//            }
//            self.downloadArray {stickers, error in
//                if let error = error {
//                    reqError = error
//                } else {
//                    stickersArray = stickers
//                }
//                group.leave()
//            }
//        }
//        DispatchQueue.global().async {
//            group.enter()
//            if stickerList != nil {
//                group.leave()
//                return
//            }
//            self.downloadList {list, error in
//                if let error = error {
//                    reqError = error
//                } else {
//                    stickerList = list
//                }
//                group.leave()
//            }
//        }
//        group.notify(queue: DispatchQueue.main, work: DispatchWorkItem(block: {
//            if let error = reqError {
//                complitionHandler(nil, false, error)
//            } else {
//                if stickerList == nil || stickersArray == nil {
//                    complitionHandler(nil, false, nil)
//                    return
//                }
//                UserDefaults.standard.set(RemoteConfig.remoteConfig()[self.stickersKey].stringValue, forKey: self.stickersKey)
//                let orderArray = self.orderArray(array: stickersArray!, order: stickerList!)
//                complitionHandler(orderArray, false, nil)
//            }
//        }))
    }
    
    fileprivate func downloadArray(comlitionHandler: @escaping (_ stickers: Array<StickerAlbum>?, _ error: Error?)->()) {
        let storage = Storage.storage()
        let pathReference = storage.reference(withPath: "stickersData.json")
        pathReference.getData(maxSize: 1 * 1024 * 1024) { (data, error) in
            if let error = error {
                comlitionHandler(nil, error)
            } else {
                let stickersData = try? JSONSerialization.jsonObject(with: data!, options: .mutableLeaves)
                self.saveArrayInUserDef(data: stickersData as! Dictionary<String, Any>)
                let array = self.stickerParser.parse(stickerData: stickersData as! Dictionary<String, Any>)
                print(array)
                comlitionHandler(array, nil)
            }
        }
    }
    
    fileprivate func downloadList(comlitionHandler: @escaping (_ list: Array<String>?, _ error: Error?)->()) {
        let storage = Storage.storage()
        let pathReference = storage.reference(withPath: "stickerList.json")
        pathReference.getData(maxSize: 1 * 1024 * 1024) { (data, error) in
            if let error = error {
                comlitionHandler(nil, error)
            } else {
                let stickerslist = try? JSONSerialization.jsonObject(with: data!, options: .mutableLeaves)
                self.saveListInUserDef(array: stickerslist as! Array<String>)
                comlitionHandler(stickerslist as? Array<String>, nil)
            }
        }
    }
    
    fileprivate func orderArray(array: Array<StickerAlbum>, order:Array<String>) -> Array<StickerAlbum> {
        var orderArray = Array<StickerAlbum>()
        
        for orderAlbumName in order {
            for album in array {
                if orderAlbumName == album.albumName {
                    orderArray.append(album)
                    break
                }
            }
        }
        
        return orderArray
    }
    
    fileprivate func downloadTestArray(comlitionHandler: @escaping GetStickersComplitionHandler) {
        let storage = Storage.storage()
        let pathReference = storage.reference(withPath: "testStickersData.json")
        pathReference.getData(maxSize: 1 * 1024 * 1024) { (data, error) in
            if let error = error {
                comlitionHandler(nil, true, error)
            } else {
                let stickersData = try? JSONSerialization.jsonObject(with: data!, options: .mutableLeaves)
                let array = self.stickerParser.parse(stickerTestData: stickersData as! Array<Dictionary<String, Any>>)
                print(array)
                comlitionHandler(array, true, nil)
            }
        }
    }
    
    fileprivate func getArrayFromUserDef() -> Array<StickerAlbum>? {
        if UserDefaults.standard.object(forKey: arrayKey) != nil {
            let array = self.stickerParser.parse(stickerData: UserDefaults.standard.object(forKey: arrayKey) as! Dictionary<String, Any>)
            return array
        }
        return nil
    }
    
    fileprivate func saveArrayInUserDef(data: Dictionary<String, Any>) {
        UserDefaults.standard.set(data, forKey: arrayKey)
    }
    
    fileprivate func getListFromUserDef() -> Array<String>? {
        if UserDefaults.standard.object(forKey: listKey) != nil {
            return UserDefaults.standard.object(forKey: listKey) as? Array<String>
        }
        return nil
    }
    
    fileprivate func saveListInUserDef(array: Array<String>) {
        UserDefaults.standard.set(array, forKey: listKey)
    }
    
    fileprivate func getStickersFromUDIfNeeded() -> (stickers: Array<StickerAlbum>?, list: Array<String>?) {
        var stickersArray: Array<StickerAlbum>?
        var stickerList: Array<String>?
        
        let stickerVersion = UserDefaults.standard.string(forKey: stickersKey)
        let remoteStickerVersion = RemoteConfig.remoteConfig()[stickersKey].stringValue
        if stickerVersion == remoteStickerVersion {
            stickersArray = getArrayFromUserDef()
            stickerList = getListFromUserDef()
        }
        
        return (stickersArray, stickerList)
    }
}

//
//  StickersInfoServiceProtocol.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 17/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

protocol StickersInfoServiceProtocol {
    func getStickers(complitionHandler: @escaping GetStickersComplitionHandler)
}

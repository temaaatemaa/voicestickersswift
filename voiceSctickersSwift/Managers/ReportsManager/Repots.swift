//
//  Repots.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 22/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit
import Firebase
import YandexMobileMetrica

class Reports {
    class func logEvent(_ name: String, parameters: [String: Any]?) {
        Analytics.logEvent(name, parameters: parameters)
        YMMYandexMetrica.reportEvent(name, parameters: parameters)
    }
    
    class func logError(_ name: String, error: Error?) {
        self.logEvent(name, parameters: ["error" : error?.localizedDescription ?? ""])
        YMMYandexMetrica.reportEvent(name, parameters: ["error" : error?.localizedDescription ?? ""])
    }
    
    class func logError(_ name: String, error: Error?, parameters: [String: Any]?) {
        var parametersNew = [String: Any]()
        parametersNew["error"] = error?.localizedDescription ?? ""
        if parameters != nil {
            parametersNew.merge(parameters!, uniquingKeysWith: { (key1, key2) -> Any in key1 })
        }
        self.logEvent(name, parameters: parametersNew)
        YMMYandexMetrica.reportEvent(name, parameters: parametersNew)
    }
}

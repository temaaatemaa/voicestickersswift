//
//  VKSendMessageManager.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 07/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit
import AFNetworking
import VK_ios_sdk
import Firebase

class VKSendMessageManager: VKBaseManager, VKSendMessageManagerProtocol {

    var userId: Int?
    var delegate: VKSendMessageManagerDelegate?
    var peerId: String {
        get {
            let peerId = String(userId!)
            return peerId
        }
    }
    
    func send(file: Data, to member: VKMemberProtocol) {
        userId = member.id
        
        send(file: file)
    }
    
    fileprivate func send(file: Data) {
        urlForUpload { (url) in
            self.upload(file: file, to: url, complitionHandler: { (file) in
                self.save(file: file, complitionHandler: { (ownerId, docId) in
                    self.send(ownerId: ownerId, docId: docId, complitionHandler: {
                        self.delegate?.didSendMessage()
                    })
                })
            })
        }
    }
    
    fileprivate func urlForUpload(complitionHandler: @escaping (_ url: String) -> ()) {
        let requestUrl = "https://api.vk.com/method/docs.getMessagesUploadServer"
        
        networkingManager.get(requestUrl, parameters: ["peer_id" : peerId,
                                                       "access_token" : accessToken,
                                                       "type" : "audio_message",
													   "v" : "5.90"], headers: nil, progress: nil, success: { (task, response) in
            print("RESPONSE:\(response ?? "")")
            let error = VKErrorHelper.errorFromResponse(response: response as! [String : Any])
            if let error = error {
                self.delegate?.didReceive(error: error)
                Reports.logError("vksendmanager_upload_error", error: error)
                return
            } else {
                complitionHandler((response as! Dictionary<String, Dictionary<String, String>>)["response"]!["upload_url"]!)
            }
        }) { (task, error) in
            self.delegate?.didReceive(error: error)
        }
    }
    
    fileprivate func upload(file: Data, to url: String, complitionHandler: @escaping (_ file: String) -> ()) {
		networkingManager.post(url, parameters: nil, headers: nil, constructingBodyWith: { (formData) in
            formData.appendPart(withFileData: file, name: "file", fileName: "file", mimeType: "file")
        }, progress: nil, success: { (task, response) in
            print(response ?? "")
            let error = VKErrorHelper.errorFromResponse(response: response as! [String : Any])
            if let error = error {
                self.delegate?.didReceive(error: error)
                Reports.logError("vksendmanager_upload_error", error: error)
                return
            } else {
				if let dict = response as? [String: Any], let file = dict["file"] as? String {
					complitionHandler(file)
					Reports.logEvent("vksendmanager_upload", parameters: dict)
				} else {
					Reports.logError("vksendmanager_upload_no_dict", error: nil)
				}
            }
        }) { (task, error) in
            self.delegate?.didReceive(error: error)
        }
    }
    
    fileprivate func save(file: String, complitionHandler: @escaping (_ ownerId: String, _ docId: String) -> ()) {
        let saveUrl = "https://api.vk.com/method/docs.save"
        let parameters = ["file": file,
                          "access_token": VKSdk.accessToken()?.accessToken,
                          "v":"5.90"]
        networkingManager.get(saveUrl,
							  parameters: parameters, headers: nil,
                              progress: nil, success: { (task, response) in
                                if let response = response as? Dictionary<String, Dictionary<String, Any>> {
                                    print(response)
                                    Reports.logEvent("vksendmanager_save", parameters: response)
                                    
                                    let error = VKErrorHelper.errorFromResponse(response: response)
                                    if let error = error {
                                        self.delegate?.didReceive(error: error)
                                        Reports.logError("vksendmanager_save_error", error: error)
                                        return
                                    }
                                    
                                    let audioInfo = response["response"]!["audio_message"]
                                    let ownerId = String((audioInfo as! Dictionary<String, Any>)["owner_id"] as! Int)
                                    let docId = String((audioInfo as! Dictionary<String, Any>)["id"] as! Int)
                                    complitionHandler(ownerId, docId)
                                } else {
                                    let error = NSError(domain: "1", code: 123, userInfo: [NSLocalizedDescriptionKey : "Не удалось схранить файл на сервере, попробуйте еще раз"])
                                    self.delegate?.didReceive(error: error)
                                }
        }) { (task, error) in
            self.delegate?.didReceive(error: error)
        }
    }
    
    fileprivate func send(ownerId: String, docId: String, complitionHandler: @escaping ()->()) {
        let sendUrl = "https://api.vk.com/method/messages.send"
        let attachment = "doc\(ownerId)_\(docId)"
        let parameters = ["peer_id": peerId,
                          "attachment": attachment,
                          "access_token": VKSdk.accessToken()?.accessToken,
                          "v": "5.81"]
		networkingManager.get(sendUrl, parameters: parameters, headers: nil, progress: nil, success: { (task, response) in
            Reports.logEvent("vksendmanager_send", parameters: response as? [String : Any])
            print(response ?? "ERROR")
            if let response = response as? [String : Any] {
                let error = VKErrorHelper.errorFromResponse(response: response)
                if let error = error {
                    self.delegate?.didReceive(error: error)
                    Reports.logError("vksendmanager_send_error", error: error)
                } else {
                    complitionHandler()
                }
            }
           
        }) { (task, error) in
            self.delegate?.didReceive(error: error)
        }
    }
}

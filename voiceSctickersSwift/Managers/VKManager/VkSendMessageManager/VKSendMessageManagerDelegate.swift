//
//  VKSendMessageManagerDelegate.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 07/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

protocol VKSendMessageManagerDelegate {
    func didReceive(error: Error)
    func didSendMessage()
}

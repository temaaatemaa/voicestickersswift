//
//  VKSendMessageManagerProtocol.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 07/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

protocol VKSendMessageManagerProtocol {
    func send(file: Data, to member: VKMemberProtocol)
    var delegate: VKSendMessageManagerDelegate? { get set }
}

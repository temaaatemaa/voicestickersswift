//
//  VKBaseManagerProtocol.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 08/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import AFNetworking

protocol VKBaseManagerProtocol {
    var networkingManager: AFHTTPSessionManager { get }
    var accessToken: String? { get }
}

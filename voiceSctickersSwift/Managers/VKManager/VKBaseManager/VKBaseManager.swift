//
//  VKBaseManager.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 08/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit
import AFNetworking
import VK_ios_sdk

class VKBaseManager: VKBaseManagerProtocol {
    var networkingManager: AFHTTPSessionManager {
        get {
            return AFHTTPSessionManager.init()
        }
    }
    var accessToken: String? {
        get {
            return VKSdk.accessToken()?.accessToken
        }
    }

}

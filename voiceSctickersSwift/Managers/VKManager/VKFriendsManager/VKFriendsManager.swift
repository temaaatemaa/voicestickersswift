//
//  VKFriendsManager.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 08/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

class VKFriendsManager: VKBaseManager, VKFriendsManagerProtocol {

    let usersParser: VKUserParserProtocol = VKUserParser()
    let groupsParser: VKGroupParserProtocol = VKGroupParser()
    
    func getFriends(complitionHandler: @escaping (Array<VKUser>?, _ error: Error?) -> ()) {
        if accessToken == nil {
            return
        }
        let parameters = ["order":"hints",
                          "fields":"photo_100",
                          "access_token":accessToken,
                          "v":"5.81"]
        let requestUrl = "https://api.vk.com/method/friends.get"
		networkingManager.get(requestUrl, parameters: parameters, headers: nil, progress: nil, success: { (task, response) in
            let friends = self.usersParser.parse(usersData: (response as! Dictionary<String, Dictionary<String, Any>>)["response"]!)
            complitionHandler(friends, nil)
        }) { (task, error) in
            complitionHandler(nil, error)
        }
    }
    
    func getInfo(forUsers users: Array<VKUser>, complitionHandler: @escaping (Array<VKUser>?, _ error: Error?) -> ()) {
        if accessToken == nil {
            return
        }
        var usersIds = String()
        for user in users {
            usersIds.append(String(user.id!))
            usersIds.append(",")
        }
        
        let parameters = ["user_ids":usersIds,
                          "fields":"photo_100,online",
                          "access_token":accessToken,
                          "v":"5.81"]
        let requestUrl = "https://api.vk.com/method/users.get"
		networkingManager.get(requestUrl, parameters: parameters, headers: nil, progress: nil, success: { (task, response) in
            let friends = self.usersParser.parse(usersData: (response as! Dictionary<String, Array<Any>>)["response"]!)
            complitionHandler(friends, nil)
        }) { (task, error) in
            complitionHandler(nil, error)
        }
    }

}

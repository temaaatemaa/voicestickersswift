//
//  VKFriendsManagerProtocol.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 08/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

protocol VKFriendsManagerProtocol {
    func getFriends(complitionHandler: @escaping (Array<VKUser>?, _ error: Error?) -> ())
    func getInfo(forUsers users: Array<VKUser>, complitionHandler: @escaping (Array<VKUser>?, _ error: Error?) -> ())
}

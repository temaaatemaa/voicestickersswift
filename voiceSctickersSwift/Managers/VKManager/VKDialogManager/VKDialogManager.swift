//
//  VKDialogManager.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 08/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

class VKDialogManager: VKBaseManager, VKDialogManagerProtocol {
    
    let dialogParser: VKDialogParserProtocol = VKDialogParser()
    
    func getDialogs(with offset: Int, complitionHandler: @escaping (Array<VKDialog>?, _ error: Error?) -> ()) {
        if accessToken == nil {
            return
        }
        let parameters = ["offset": String(offset),
                          "count": "50",
                          "extended": "1",
                          "access_token": accessToken,
                          "v": "5.81"]
        
        let requestUrl = "https://api.vk.com/method/messages.getConversations"
		networkingManager.get(requestUrl, parameters: parameters, headers: nil, progress: nil, success: { (task, response) in
            let error = VKErrorHelper.errorFromResponse(response: response as! [String : Any])
            if let error = error {
                complitionHandler(nil, error)
                Reports.logError("getDialogs_error", error: error)
                return
            } else {
                let dialogs = self.dialogParser.parse(dialogsData: (response as! Dictionary<String, Dictionary<String, Any>>)["response"]!)
                complitionHandler(dialogs, nil)
            }
        }) { (task, error) in
            complitionHandler(nil, error)
        }
    }
}

//
//  VKDialogManagerProtocol.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 08/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

protocol VKDialogManagerProtocol {
    func getDialogs(with offset: Int, complitionHandler: @escaping (Array<VKDialog>?, _ error: Error?) -> ())
}

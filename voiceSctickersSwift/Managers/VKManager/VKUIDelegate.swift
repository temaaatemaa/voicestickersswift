//
//  VKUIDelegate.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 07/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit
import VK_ios_sdk

class VKUIDelegate: NSObject, VKSdkUIDelegate {
    
    var viewController: UIViewController?
    
    func vkSdkShouldPresent(_ controller: UIViewController!) {
        self.viewController?.present(controller, animated: true, completion: nil)
    }
    
    func vkSdkNeedCaptchaEnter(_ captchaError: VKError!) {
    }
    
}

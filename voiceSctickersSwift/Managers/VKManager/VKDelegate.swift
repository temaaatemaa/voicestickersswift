//
//  VKDelegate.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 07/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit
import VK_ios_sdk

class VKDelegate: NSObject, VKSdkDelegate {
    func vkSdkAccessAuthorizationFinished(with result: VKAuthorizationResult!) {
    }
    
    func vkSdkUserAuthorizationFailed() {
    }
}

//
//  VKServiceProtocol.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 07/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

protocol VKServiceProtocol {
    var isLogin: Bool { get }
    func autentification(from viewController: UIViewController)
    func getUserInfo(complitionHandler: @escaping (_ firstName: String?, _ lastName: String?, _ error: Error?) -> ())
    func logout()
}

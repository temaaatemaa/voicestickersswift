//
//  VKApi.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 07/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit
import VK_ios_sdk
import Firebase

class VKService: VKServiceProtocol {
    var isLogin: Bool {
        get {
            return VKSdk.isLoggedIn()
        }
    }

    func autentification(from viewController: UIViewController) {
        
        let sdkInstance = VKSdk.initialize(withAppId: "6400276")
        let uiDelegate = VKUIDelegate()
        let delegate = VKDelegate()
        
        
        uiDelegate.viewController = viewController
        
        sdkInstance?.uiDelegate = uiDelegate
        sdkInstance?.register(delegate)
        
        let scope = ["friends", "messages", "docs"]
        
        VKSdk.wakeUpSession(scope) { (state, error) in
            switch state {
            case .initialized: do {
                VKSdk.authorize(scope)
                break;
                }
            case .unknown: break
            case .pending: break
            case .external: break
            case .safariInApp: break
            case .webview: break
            case .authorized: break
            case .error:
                if let error = error {
                    print(error)
                    Reports.logError("vk_autentification_error", error: error)
                }
            }
        }
    }
    
    func getUserInfo(complitionHandler: @escaping (_ firstName: String?, _ lastName: String?, _ error: Error?) -> ()) {
        let request = VKApi.request(withMethod: "account.getProfileInfo", andParameters: [:])
        request?.execute(resultBlock: { (responce) in
            let responseDict = VKService.parseResponse(response: responce!)
            let name = responseDict["first_name"] as? String
            let surname = responseDict["last_name"] as? String
            complitionHandler(name, surname, nil)
            Reports.logEvent("vk_get_user_info", parameters: ["name" : "\(name ?? "") \(surname ?? "")"])
        }, errorBlock: { (error) in
            complitionHandler(nil, nil, error)
            Reports.logError("vk_get_user_info_error", error: error)
        })
    }
    
    class func parseResponse(response: VKResponse<VKApiObject>) -> Dictionary<String, Any> {
        let data = try? JSONSerialization.data(withJSONObject: response.json, options: .prettyPrinted)
        let dictionary = try? JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
        
        return dictionary as! Dictionary<String, Any>
    }
    
    func logout() {
        VKSdk.forceLogout()
        Reports.logEvent("vk_logout", parameters: nil)
    }
}

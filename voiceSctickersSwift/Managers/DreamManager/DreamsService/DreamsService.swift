//
//  DreamsService.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 20/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit
import Firebase

class DreamsService: DreamsServiceProtocol {
    var db: Firestore {
        get { return Firestore.firestore() }
    }

    public func addDream(name :String,
                         completionHandler: @escaping (_ error : Error?) -> Void) {
        db.collection("dreams").addDocument(data: ["sticker": name, "voted": 0]) { (error) in
            completionHandler(error)
            Reports.logEvent("dream_servise_add", parameters: ["error" : error?.localizedDescription ?? ""])
        }
    }
    
    public func getDreams(complitionHandler: @escaping (_ dreams: [Dream]?, _ error: Error?)->()) {
		let limit = RemoteConfig.remoteConfig()["dreams_limit_download"].numberValue.intValue
        let dreams = db.collection("dreams").order(by: "voted", descending: true).limit(to: limit)
        dreams.getDocuments(completion: { (querySnapshot, error) in
            if let error = error {
                print("Error getting documents: \(error)")
                complitionHandler(nil, error)
                Reports.logError("dream_servise_get_error", error: error)
            } else {
                var dreams = [Dream]()
                for document in querySnapshot!.documents {
                    let dream = Dream.init(id: document.documentID,
                                           name: document.data()["sticker"] as? String,
                                           votedCount: document.data()["voted"] as? Int)
                
                    dreams.append(dream)
                }
                complitionHandler(dreams, nil)
                Reports.logEvent("dream_servise_get", parameters: ["count" : dreams.count])
            }
        })
    }
    
    public func voteFor(dream: Dream, completionHandler: @escaping (_ error : Error?) -> Void) {
        let dreams = db.collection("dreams").document(dream.id!)
        let asd = FieldValue.increment(1 as Double)
        dreams.updateData(["voted" : asd]) { (error) in
            completionHandler(error)
            Reports.logEvent("dream_servise_vote", parameters: ["sticker" : dream.name ?? ""])
        }
    }
}

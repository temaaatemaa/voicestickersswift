//
//  DreamManager.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 20/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit
import Firebase

class DreamManager: DreamManagerProtocol {
    let service: DreamsServiceProtocol = DreamsService()
    let votesKey = "-votesRemaining"
    let addKey = "-newStickersRemaining"
    let updateKey = "-updateRemaining"
    let saveDreamsKey = "dreamsCache"
    
    var todayKey: String {
        get {
            let date = Date()
            let calendar = Calendar.current
            let day = calendar.component(.day, from: date)
            let month = calendar.component(.month, from: date)
            let year = calendar.component(.year, from: date)
            return "\(day).\(month).\(year)"
        }
    }
    
    var votesRemaining: Int {
        get {
            let count = UserDefaults.standard.object(forKey: todayKey + votesKey) as? Int
            if count == nil {
				let firebase = RemoteConfig.remoteConfig()["votes_count"].numberValue.intValue
                UserDefaults.standard.set(firebase, forKey: todayKey + votesKey)
            }
            
            return UserDefaults.standard.integer(forKey: todayKey + votesKey)
        }
    }
    
    var newStickersRemaining: Int {
        get {
            let count = UserDefaults.standard.object(forKey: todayKey + addKey) as? Int
            if count == nil {
				let firebase = RemoteConfig.remoteConfig()["add_stickers_count"].numberValue.intValue
                UserDefaults.standard.set(firebase, forKey: todayKey + addKey)
            }
            
            return UserDefaults.standard.integer(forKey: todayKey + addKey)
        }
    }
    
    var updatesRemaining: Int {
        get {
            let count = UserDefaults.standard.object(forKey: todayKey + updateKey) as? Int
            if count == nil {
				let firebase = RemoteConfig.remoteConfig()["updates_count"].numberValue.intValue
                UserDefaults.standard.set(firebase, forKey: todayKey + updateKey)
            }
            
            return UserDefaults.standard.integer(forKey: todayKey + updateKey)
        }
    }
    
    func addDream(name: String, completionHandler: @escaping (Error?) -> Void) {
        if name.count < 4 {
            let error = NSError(domain:"", code:0, userInfo:[NSLocalizedDescriptionKey: "Слишком маленькое название"])
            completionHandler(error)
            Reports.logError("dream_vote_error", error: error)
            return
        }
        UserDefaults.standard.set(newStickersRemaining - 1, forKey: todayKey + addKey)
        service.addDream(name: name, completionHandler: completionHandler)
    }
    
    func getDreams(complitionHandler: @escaping ([Dream]?, Error?) -> ()) {
        if updatesRemaining == 0 {
            let dreams = UserDefaults.standard.value([Dream].self, forKey: self.saveDreamsKey)
            complitionHandler(dreams, nil)
            Reports.logEvent("dream_load_from_UD", parameters: ["count" : dreams?.count ?? 0])
            return
        }
        UserDefaults.standard.set(updatesRemaining - 1, forKey: todayKey + updateKey)
        service.getDreams { (dreams, error) in
            if let error = error {
                complitionHandler(nil, error)
                Reports.logError("dream_get_error", error: error)
            } else {
                UserDefaults.standard.set(encodable: dreams, forKey: self.saveDreamsKey)
                complitionHandler(dreams, nil)
                Reports.logEvent("dream_load", parameters: ["count" : dreams?.count ?? 0])
            }
        }
    }
    
    func voteFor(dream: Dream, completionHandler: @escaping (Error?) -> Void) {
        UserDefaults.standard.set(votesRemaining - 1, forKey: todayKey + votesKey)
        service.voteFor(dream: dream, completionHandler: completionHandler)
        Reports.logEvent("dream_vote", parameters: ["sticker" : dream.name ?? ""])
    }
}

//
//  DreamManagerProtocol.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 21/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

protocol DreamManagerProtocol {
    var votesRemaining: Int { get }
    var newStickersRemaining: Int { get }
    
    func addDream(name :String,
                  completionHandler: @escaping (_ error : Error?) -> Void)
    func getDreams(complitionHandler: @escaping (_ dreams: [Dream]?, _ error: Error?)->())
    func voteFor(dream: Dream, completionHandler: @escaping (_ error : Error?) -> Void)
}

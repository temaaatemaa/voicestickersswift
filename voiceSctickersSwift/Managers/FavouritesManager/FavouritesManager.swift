//
//  FavouritesManager.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 28/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

class FavouritesManager: FavouritesManagerProtocol {
    let stickerHelper = StickerHelper()
    var favourites: [FavouriteSticker] {
        get {
            let array = UserDefaults.standard.value([FavouriteSticker].self, forKey: "FavouritesManager.stickers")
            if let array = array {
                return array
            } else {
                return [FavouriteSticker]()
            }
        }
        set {
            UserDefaults.standard.set(encodable: newValue, forKey: "FavouritesManager.stickers")
        }
    }
    
    func addFavourites(sticker: StickerProtocol, from album: StickerAlbumProtocol) {
        let isYetFavourite = stickerHelper.isStickerFavourite(sticker: sticker, from: album)
        
        var favourite: FavouriteSticker!
        if album is TestStickerAlbumProtocol {
            let testSticker = sticker as! TestStickerProtocol
            favourite = FavouriteSticker(albumNumber: testSticker.albumNumber,
                                         soundNumber: testSticker.soundNumber,
                                         name: (testSticker as! StickerProtocol).name)
        } else {
            let album = album as! StickerAlbum
            let index = stickerHelper.stickerSoundNumber(sticker: sticker as! Sticker, from: album)
            guard (index != nil) else {
                return
            }
            favourite = FavouriteSticker(albumNumber: album.albumNumber, soundNumber: index!, name: sticker.name)
        }
        if isYetFavourite {
            removeFavourite(sticker: favourite)
        } else {
           favourites.append(favourite)
        }
    }
    
    func removeFavourite(sticker: FavouriteSticker) {
        let arr = favourites
        let index = arr.firstIndex { (favSticker) -> Bool in
            if sticker.name == favSticker.name &&
                sticker.albumNumber == favSticker.albumNumber &&
                sticker.soundNumber == favSticker.soundNumber {
                return true
            }
            return false
        }
        
        if let index = index {
            favourites.remove(at: index)
        }
    }
    
    func getFavouritesAlbum() -> FavouriteAlbum? {
        if favourites.count == 0 {
            return nil
        }
        return FavouriteAlbum(albumName: "Избранное",
                              albumPreviewUrl: "https://firebasestorage.googleapis.com/v0/b/voicestickers.appspot.com/o/album_fav.png?alt=media&token=d41b0ccc-4635-414d-b084-4a969f6a61d1",
                              paid: false,
                              stickers: favourites)
    }
    
    func getFavouritesStickers() -> [FavouriteSticker] {
        return favourites
    }
}

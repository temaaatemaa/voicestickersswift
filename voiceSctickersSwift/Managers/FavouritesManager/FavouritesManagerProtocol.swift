//
//  FavouritesManagerProtocol.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 28/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

protocol FavouritesManagerProtocol {
    func addFavourites(sticker: StickerProtocol, from album: StickerAlbumProtocol)
    func getFavouritesAlbum() -> FavouriteAlbum?
    func getFavouritesStickers() -> [FavouriteSticker]
}

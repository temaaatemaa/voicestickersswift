//
//  OwnStickerManager.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 12.04.2020.
//  Copyright © 2020 n. All rights reserved.
//

import UIKit

protocol OwnStickerManagerProtocol {
    func save(sticker id: String, name: String, changed: Bool)
    func removeSticker(sticker: OwnSticker)
    func getOwnStickersAlbum() -> OwnStickerAlbum?
	func getOwnStickers() -> [OwnSticker]
}

class OwnStickerManager: OwnStickerManagerProtocol {

	var ownStickers: [OwnSticker] {
        get {
            let array = UserDefaults.standard.value([OwnSticker].self, forKey: "OwnStickerManager.stickers")
            if let array = array {
                return array
            } else {
                return [OwnSticker]()
            }
        }
        set {
            UserDefaults.standard.set(encodable: newValue, forKey: "OwnStickerManager.stickers")
        }
    }

	func save(sticker id: String, name: String, changed: Bool) {
		let sticker = OwnSticker(id: id, name: name, changed: changed)
		let existedIndex = ownStickers.firstIndex(where: { $0.changed == changed && $0.id == id})
		if let index = existedIndex {
			ownStickers.remove(at: index)
		}
		ownStickers.append(sticker)
    }

    func removeSticker(sticker: OwnSticker) {
        let arr = ownStickers
        let index = arr.firstIndex { (favSticker) -> Bool in
            if sticker.id == favSticker.id {
                return true
            }
            return false
        }

        if let index = index {
            ownStickers.remove(at: index)
        }
    }

    func getOwnStickersAlbum() -> OwnStickerAlbum? {
        if ownStickers.count == 0 {
            return nil
        }
        return OwnStickerAlbum(albumName: "Мои стикеры", albumPreviewUrl: "https://firebasestorage.googleapis.com/v0/b/voicestickers.appspot.com/o/album_mic.png?alt=media&token=654ecf44-8bbc-4c06-9825-4490c6332dcd", paid: false, stickers: ownStickers)
    }

    func getOwnStickers() -> [OwnSticker] {
        return ownStickers
    }

	var newId: String {
		randomString(length: 20)
	}

	func randomString(length: Int) -> String {
	  let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	  return String((0..<length).map{ _ in letters.randomElement()! })
	}
}

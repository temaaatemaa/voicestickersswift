//
//  StickerManager.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 07/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

class StickerManager: StickerManagerProtocol {
    
    let fileManager: StickerFileManagerProtocol = StickerFileManager()
    let downloadManager: StickerDownloaderManagerProtocol = StickerDownloaderManager()
    let stickerPlayer: StickerPlayerManagerProtocol = StickerPlayerManager()
    let stickerChanger = StickerChangeManager()
    let stickerConverter = StickerConverterManager()

    
    func play(sticker: StickerProtocol, from album: StickerAlbumProtocol) {
		if let sticker = sticker as? OwnSticker {
			var stickerData: Data?
			if sticker.changed {
				stickerData = fileManager.getChangedRecordedFile(for: sticker.id)
			} else {
				stickerData = fileManager.getRecordedFile(for: sticker.id)
			}

			guard let file = stickerData else { return }
			self.stickerPlayer.play(file: file)
			return
		}
        getMp3File(sticker: sticker, from: album) { (file, error) in
            if let error = error {
                Reports.logError("StickerManager_play_error", error: error)
            } else {
                self.stickerPlayer.play(file: file!)
            }
        }
    }
    
    func stopPlay() {
        stickerPlayer.stop()
    }
    
    func getMp3File(sticker: StickerProtocol, from album: StickerAlbumProtocol, complitionHandler: @escaping (Data?, Error?) -> ()) {
        let file = fileManager.getMp3File(of: sticker, from: album)
        if file == nil {
            downloadManager.downloadMp3(sticker: sticker, from: album) { (file, error) in
                if let error = error {
                    complitionHandler(nil, error)
                } else {
                    self.fileManager.saveMp3File(file: file!, of: sticker, from: album)
                    complitionHandler(file, nil)
                }
            }
        } else {
            complitionHandler(file, nil)
        }
    }
    
    func getData(sticker: StickerProtocol, from album: StickerAlbumProtocol, complitionHandler: @escaping (Data?, Error?) -> ()) {
        let filePath = fileManager.getMp3FilePath(of: sticker, from: album)
        let oggFilePath = fileManager.getOggFilePath(of: sticker, from: album)
        stickerConverter.convert(for: filePath, oggFilePath: oggFilePath) {
            let file = self.fileManager.getOggFile(of: sticker, from: album)
            complitionHandler(file, nil)
        }
    }

	func getOwnStickerFile(ownSticker: OwnSticker) -> Data? {
		if ownSticker.changed == true {
			return fileManager.getChangedRecordedFile(for: ownSticker.id)
		} else {
			return fileManager.getRecordedFile(for: ownSticker.id)
		}
    }

	func getOwnStickerFileUrl(ownSticker: OwnSticker) -> URL? {
		if ownSticker.changed == true {
			return URL(string: fileManager.getChangedRecordedFilePath(for: ownSticker.id))
		} else {
			return URL(string: fileManager.getRecordedFilePath(for: ownSticker.id))
		}

    }
    
    func changeStickerAndPlay(sticker: StickerProtocol, from album: StickerAlbumProtocol, with pitch: Float, speed: Float) {
        let filePath = fileManager.getMp3FilePath(of: sticker, from: album)
        let newFilePath = fileManager.getFilePathForChangedMp3File(of: sticker, from: album)
        
        stickerChanger.changePitch(for: filePath, savePath: newFilePath, pitch: pitch, speed: speed) {
            let file = self.getChangedMp3File(sticker: sticker, from: album)
            self.stickerPlayer.play(file: file!)
        }
    }

	func changeStickerAndPlay(ownSticker id: String, with pitch: Float, speed: Float) {
        let filePath = fileManager.getRecordedFilePath(for: id)
        let newFilePath = fileManager.getChangedRecordedFilePath(for: id)

        stickerChanger.changePitch(for: filePath, savePath: newFilePath, pitch: pitch, speed: speed) {
			let file = self.fileManager.getChangedRecordedFile(for: id)
            self.stickerPlayer.play(file: file!)
        }
    }
    
    func getChangedMp3File(sticker: StickerProtocol, from album: StickerAlbumProtocol) -> Data? {
        let file = self.fileManager.getChangedMp3File(of: sticker, from: album)
        return file
    }
    
    
    func getChangedData(sticker: StickerProtocol, from album: StickerAlbumProtocol, complitionHandler: @escaping (Data?, Error?) -> ()) {
        let filePath = fileManager.getChangedMp3FilePath(of: sticker, from: album)
        let oggFilePath = fileManager.getOggFilePath(of: sticker, from: album)
        stickerConverter.convert(for: filePath, oggFilePath: oggFilePath) {
            let file = self.fileManager.getOggFile(of: sticker, from: album)
            complitionHandler(file, nil)
        }
    }
    
    func isStickerDownloaded(sticker: StickerProtocol, from album: StickerAlbumProtocol) -> Bool {
		if sticker is OwnSticker {
			return true
		}
        let file = fileManager.getMp3File(of: sticker, from: album)
        if file == nil {
            return false
        }
        return true
    }
    
    func download(sticker: StickerProtocol, from album: StickerAlbumProtocol, complitionHandler: @escaping (_ error: Error?) -> ()) {
        downloadManager.downloadMp3(sticker: sticker, from: album) { (file, error) in
            if let error = error {
                print(error)
                complitionHandler(error)
            } else {
                self.fileManager.saveMp3File(file: file!, of: sticker, from: album)
                complitionHandler(nil)
            }
        }
    }
    
    func download(album: StickerAlbumProtocol, prtogressHandler: @escaping (_ sticker: StickerProtocol)->(), complitionHandler: @escaping (_ error: Error?)->()) {
        let group = DispatchGroup.init()
        var errorReq: Error?
        for sticker in album.stickers {
            if !isStickerDownloaded(sticker: sticker, from: album) {
                group.enter()
                self.download(sticker: sticker, from: album) { (error) in
                    if let error = error {
                        errorReq = error
                    } else {
                        prtogressHandler(sticker)
                    }
                    group.leave()
                }
            }
        }
        group.notify(queue: DispatchQueue.main) {
            complitionHandler(errorReq)
        }
    }
    
    func getChangedMp3FilePath(sticker: StickerProtocol, from album: StickerAlbumProtocol) -> URL? {
        let path = self.fileManager.getFilePathForChangedMp3File(of: sticker, from: album)
        return URL(string: path)
    }
	
    func getMp3FilePath(sticker: StickerProtocol, from album: StickerAlbumProtocol,
                        complitionHandler: @escaping (_ url: URL?) -> ()){
        self.getMp3File(sticker: sticker, from: album) { (_, error) in
            if error == nil {
                let fileurl = self.fileManager.getMp3FilePath(of: sticker, from: album)
                complitionHandler(URL(string: fileurl))
            } else {
                complitionHandler(nil)
            }
        }
    }
}

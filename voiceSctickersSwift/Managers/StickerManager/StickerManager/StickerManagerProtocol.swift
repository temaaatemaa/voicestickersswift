
//
//  StickerManagerProtocol.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 07/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

protocol StickerManagerProtocol {
    func isStickerDownloaded(sticker: StickerProtocol, from album: StickerAlbumProtocol) -> Bool
    func download(sticker: StickerProtocol, from album: StickerAlbumProtocol, complitionHandler: @escaping (_ error: Error?) -> ())
    func play(sticker: StickerProtocol, from album: StickerAlbumProtocol)
    func stopPlay()
    func getMp3File(sticker: StickerProtocol, from album: StickerAlbumProtocol, complitionHandler: @escaping (Data?, Error?) -> ())
    func getChangedMp3File(sticker: StickerProtocol, from album: StickerAlbumProtocol) -> Data?
    func getChangedMp3FilePath(sticker: StickerProtocol, from album: StickerAlbumProtocol) -> URL?
    func getMp3FilePath(sticker: StickerProtocol, from album: StickerAlbumProtocol,
                        complitionHandler: @escaping (_ url: URL?) -> ())
    
    func getData(sticker: StickerProtocol, from album: StickerAlbumProtocol, complitionHandler: @escaping (_ file: Data?, _ error: Error?) -> ())
    func getChangedData(sticker: StickerProtocol, from album: StickerAlbumProtocol, complitionHandler: @escaping (_ file: Data?, _ error: Error?) -> ())
    func changeStickerAndPlay(sticker: StickerProtocol, from album: StickerAlbumProtocol, with pitch: Float, speed: Float)
    func download(album: StickerAlbumProtocol, prtogressHandler: @escaping (_ sticker: StickerProtocol)->(), complitionHandler: @escaping (_ error: Error?)->())

	func changeStickerAndPlay(ownSticker id: String, with pitch: Float, speed: Float)
	func getOwnStickerFile(ownSticker: OwnSticker) -> Data?
	func getOwnStickerFileUrl(ownSticker: OwnSticker) -> URL?
}


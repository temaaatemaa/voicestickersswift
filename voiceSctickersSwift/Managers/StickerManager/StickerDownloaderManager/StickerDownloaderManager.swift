//
//  StickerDownloader.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 07/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit
import Firebase

class StickerDownloaderManager: StickerDownloaderManagerProtocol {
    
    func downloadMp3(sticker: StickerProtocol,
                     from album:StickerAlbumProtocol,
                     complitionHandler: @escaping (_ stickerAudioData: Data?, _ error: Error?) -> ()) {
        let soundName = StickerNameManager.fileName(for: sticker, from: album)
        let fileName = soundName?.appending(".mp3")
        download(fileName: fileName!, complitionHandler: complitionHandler)
    }
    
    func downloadOpus(sticker: StickerProtocol,
                     from album:StickerAlbumProtocol,
                     complitionHandler: @escaping (_ stickerAudioData: Data?, _ error: Error?) -> ()) {
        let soundName = StickerNameManager.fileName(for: sticker, from: album)
        let fileName = soundName?.appending(".opus")
        download(fileName: fileName!, complitionHandler: complitionHandler)
    }
    
    fileprivate func download(fileName: String,
                              complitionHandler: @escaping (_ stickerAudioData: Data?, _ error: Error?) -> ()) {
        let storage = Storage.storage()
        let folder = (fileName.contains("opus") == true) ? "stickerOpus" : "stickers"
        let pathReference = storage.reference(withPath: "\(folder)/\(fileName)")
        pathReference.getData(maxSize: 10 * 1024 * 1024) { (data, error) in
            if let error = error {
                complitionHandler(nil, error)
            } else {
                complitionHandler(data, nil)
            }
        }
    }
}

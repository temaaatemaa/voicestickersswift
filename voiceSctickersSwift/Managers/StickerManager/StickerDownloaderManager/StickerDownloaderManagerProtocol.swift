//
//  StickerDownloaderManagerProtocol.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 07/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

protocol StickerDownloaderManagerProtocol {
    func downloadMp3(sticker: StickerProtocol,
                     from album:StickerAlbumProtocol,
                     complitionHandler: @escaping (_ stickerAudioData: Data?, _ error: Error?) -> ())
    func downloadOpus(sticker: StickerProtocol,
                     from album:StickerAlbumProtocol,
                     complitionHandler: @escaping (_ stickerAudioData: Data?, _ error: Error?) -> ())
}

//
//  StickerConverterManager.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 15/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

class StickerConverterManager: NSObject {
    func convert(for mp3filePath: String, oggFilePath: String, complitionHandler: @escaping ()->()) {

        let executeString = "-i \(mp3filePath) -c:a libvorbis -q:a 0 \(oggFilePath)"
        
        DispatchQueue.global().async {
			FFmpegKit.cancel()
			FFmpegKit.execute(executeString)
            DispatchQueue.main.async {
                complitionHandler()
            }
        }
    }
}

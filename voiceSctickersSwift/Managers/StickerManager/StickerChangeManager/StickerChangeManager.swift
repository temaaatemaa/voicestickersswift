//
//  StickerChangeManager.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 15/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

class StickerChangeManager {
    func changePitch(for filePath: String, savePath: String, pitch: Float, speed: Float, complitionHandler: @escaping ()->()) {
        
        let first = pitch
        let second = speed
        
        let executeString = "-i \(filePath) -af asetrate=44100*\(first),aresample=44100,atempo=\(second),atempo=\(second) -y \(savePath)"
        
        DispatchQueue.global().async {
            FFmpegKit.cancel()
            FFmpegKit.execute(executeString)

//            print(MobileFFmpeg.getLastCommandOutput())
            DispatchQueue.main.async {
                complitionHandler()
            }
        }
    }
}

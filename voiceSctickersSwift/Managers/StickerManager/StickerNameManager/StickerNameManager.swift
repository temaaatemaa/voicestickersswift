//
//  StickerNameManager.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 07/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

class StickerNameManager: StickerNameManagerProtocol {
    
    class func fileName(for sticker: StickerProtocol, from album: StickerAlbumProtocol) -> String? {
        if let sticker = sticker as? TestStickerProtocol {
            return fileNameForTest(sticker: sticker)
        }else if let sticker = sticker as? Sticker {
            if let album = album as? StickerAlbum {
                let stickerIndex = album.stickers.firstIndex { (albumSticker) -> Bool in
                    return sticker.name == albumSticker.name
                }
                if let stickerIndex = stickerIndex {
                    return "\(album.albumNumber)_sound_\(stickerIndex)"
                }
            }
        }
        
        return nil
    }
    
    class func fileNameForTest(sticker: TestStickerProtocol) -> String {
        return "\(sticker.albumNumber)_sound_\(sticker.soundNumber)"
    }
    
}

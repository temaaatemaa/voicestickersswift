//
//  StickerPlayer.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 07/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit
import AVFoundation


class StickerPlayerManager: NSObject, StickerPlayerManagerProtocol {
    
    var player: AVAudioPlayer?
    var complitionHandler: (()->())?
	var delegate: RecorderManagerDelegate?
	public var metering: Bool = true
	private var link: CADisplayLink?

    
    func play(file: Data) {
        try? AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: .defaultToSpeaker)
        player = try? AVAudioPlayer.init(data: file)
        player?.play()
        player?.delegate = self

		player?.isMeteringEnabled = metering

		if metering {
			startMetering()
		}
    }
    
    func play(file: Data, complitionHandler: @escaping ()->()) {
        self.complitionHandler = complitionHandler
        self.play(file: file)
    }
    
    func stop() {
        player = nil
		stopMetering()
    }

	// MARK: - Metering
	@objc func updateMeter() {
		guard let player = player else { return }

		player.updateMeters()

		let dB = player.averagePower(forChannel: 0)

		delegate?.audioMeterDidUpdate(dB)
	}

	private func startMetering() {
		link = CADisplayLink(target: self, selector: #selector(updateMeter))
		link?.add(to: RunLoop.current, forMode: RunLoop.Mode.common)
	}

	private func stopMetering() {
		link?.remove(from: RunLoop.current, forMode: RunLoop.Mode.common)
		link = nil
	}
}

extension StickerPlayerManager: AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
		stopMetering()
		complitionHandler?()
    }
}

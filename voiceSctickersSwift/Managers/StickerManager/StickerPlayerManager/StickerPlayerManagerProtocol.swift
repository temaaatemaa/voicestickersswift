//
//  StickerPlayerManagerProtocol.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 07/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

protocol StickerPlayerManagerProtocol {
    func play(file: Data)
    func play(file: Data, complitionHandler: @escaping ()->())
    func stop()
}

//
//  StickerProtocolFileManagerProtocol.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 07/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

protocol StickerFileManagerProtocol {
    func getMp3File(of sticker: StickerProtocol, from album: StickerAlbumProtocol) -> Data?
    func getChangedMp3File(of sticker: StickerProtocol, from album: StickerAlbumProtocol) -> Data?//no need
    func getOpusFile(of sticker: StickerProtocol, from album: StickerAlbumProtocol) -> Data?
    func getOggFile(of sticker: StickerProtocol, from album: StickerAlbumProtocol) -> Data?
    
    func saveMp3File(file: Data, of sticker: StickerProtocol, from album: StickerAlbumProtocol)
    func saveOpusFile(file: Data, of sticker: StickerProtocol, from album: StickerAlbumProtocol)
    
    func getMp3FilePath(of sticker: StickerProtocol, from album: StickerAlbumProtocol) -> String
    func getOggFilePath(of sticker: StickerProtocol, from album: StickerAlbumProtocol) -> String
    func getChangedMp3FilePath(of sticker: StickerProtocol, from album: StickerAlbumProtocol) -> String
    
    func getFilePathForChangedMp3File(of sticker: StickerProtocol, from album: StickerAlbumProtocol) -> String

	func getRecordedFilePath(for id: String) -> String
	func getRecordedFile(for id: String) -> Data?
	func getChangedRecordedFilePath(for id: String) -> String
	func getChangedRecordedFile(for id: String) -> Data?
}

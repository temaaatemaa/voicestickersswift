//
//  StickerFileManager.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 07/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

class StickerFileManager: StickerFileManagerProtocol {
    
    let fileManager = FileManager.default
    var documentDirectory: URL? {
        get {
            return try? fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:false)
        }
    }
    
    func fileName(for sticker: StickerProtocol, from album: StickerAlbumProtocol, with fileExtension: String) -> String {
        let soundName = StickerNameManager.fileName(for: sticker, from: album)
        let fileName = soundName?.appending(fileExtension)
        return fileName!
    }
    
    func getMp3FilePath(of sticker: StickerProtocol, from album: StickerAlbumProtocol) -> String {
        let fileName = self.fileName(for: sticker, from: album, with: ".mp3")
        return (documentDirectory?.absoluteString.appending(fileName))!
    }
    
    func getOggFilePath(of sticker: StickerProtocol, from album: StickerAlbumProtocol) -> String {
        let fileName = self.fileName(for: sticker, from: album, with: ".ogg")
        return (documentDirectory?.absoluteString.appending(fileName))!
    }
    
    func getChangedMp3FilePath(of sticker: StickerProtocol, from album: StickerAlbumProtocol) -> String {
        let fileName = self.fileName(for: sticker, from: album, with: "-changed.mp3")
        return (documentDirectory?.absoluteString.appending(fileName))!
    }
    
    func getFilePathForChangedMp3File(of sticker: StickerProtocol, from album: StickerAlbumProtocol) -> String {
        let fileName = self.fileName(for: sticker, from: album, with: "-changed.mp3")
        return (documentDirectory?.absoluteString.appending(fileName))!
    }
    
    func getMp3File(of sticker: StickerProtocol, from album: StickerAlbumProtocol) -> Data? {
        let fileName = self.fileName(for: sticker, from: album, with: ".mp3")
        return getFile(fileName: fileName)
    }
    
    func getOggFile(of sticker: StickerProtocol, from album: StickerAlbumProtocol) -> Data? {
        let fileName = self.fileName(for: sticker, from: album, with: ".ogg")
        return getFile(fileName: fileName)
    }
    
    func getChangedMp3File(of sticker: StickerProtocol, from album: StickerAlbumProtocol) -> Data? {
        let fileName = self.fileName(for: sticker, from: album, with: "-changed.mp3")
        return getFile(fileName: fileName)
    }
    
    func getOpusFile(of sticker: StickerProtocol, from album: StickerAlbumProtocol) -> Data? {
        let fileName = self.fileName(for: sticker, from: album, with: ".opus")
        return getFile(fileName: fileName)
    }
    
    fileprivate func getFile(fileName: String) -> Data? {
//        let fileBundleUrl = Bundle.main.bundleURL.appendingPathComponent(fileName)
//        let data = try? Data.init(contentsOf: fileBundleUrl)
//        if let data = data {
//            try? fileManager.copyItem(at: fileBundleUrl, to: (documentDirectory?.appendingPathComponent(fileName))!)
//            return data
//        } else {
//            let fileURL = documentDirectory?.appendingPathComponent(fileName)
//            return try? Data.init(contentsOf: fileURL!)
//        }
        
        let fileURL = documentDirectory?.appendingPathComponent(fileName)
        let data = try? Data.init(contentsOf: fileURL!)
        if let data = data {
            return data
        } else {
            let fileBundleUrl = Bundle.main.bundleURL.appendingPathComponent(fileName)
            let dataBundle = try? Data.init(contentsOf: fileBundleUrl)
            if let dataBundle = dataBundle {
                try? fileManager.copyItem(at: fileBundleUrl, to: (documentDirectory?.appendingPathComponent(fileName))!)
                return dataBundle
            }
            return nil
        }
    }
    
    func saveMp3File(file: Data, of sticker: StickerProtocol, from album: StickerAlbumProtocol) {
        let fileName = self.fileName(for: sticker, from: album, with: ".mp3")
        saveFile(fileName: fileName, file: file)
    }
    
    func saveOpusFile(file: Data, of sticker: StickerProtocol, from album: StickerAlbumProtocol) {
        let fileName = self.fileName(for: sticker, from: album, with: ".opus")
        saveFile(fileName: fileName, file: file)
    }
    
    fileprivate func saveFile(fileName: String, file: Data) {
        do {
            let fileURL = documentDirectory!.appendingPathComponent(fileName)
            try file.write(to: fileURL)
        } catch {
            print(error)
        }
    }

	func getRecordedFilePath(for id: String) -> String {
        let fileName = id + "-own.flac"
        return (documentDirectory?.absoluteString.appending(fileName))!
    }

	func getChangedRecordedFilePath(for id: String) -> String {
        let fileName = id + "-own-changed.flac"
        return (documentDirectory?.absoluteString.appending(fileName))!
    }

	func getRecordedFile(for id: String) -> Data? {
		let fileName = id + "-own.flac"
		return getFile(fileName: fileName)
	}

	func getChangedRecordedFile(for id: String) -> Data? {
		let fileName = id + "-own-changed.flac"
		return getFile(fileName: fileName)
	}
}

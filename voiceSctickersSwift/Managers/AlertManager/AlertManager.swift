//
//  AlertManager.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 21/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit
import Firebase

class AlertManager {
    class func showRateAlert(from viewController: UIViewController) {
        let rateAction: AlertButtonAction = { (_) in
            let url = URL(string: "https://itunes.apple.com/us/app/appName/id1357370653?mt=8&action=write-review")
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
            Reports.logEvent("settings_rate_appstore", parameters: nil)
        }
        let alert = AlertHelper.alertThreeButtons(title: "Оценить",
                                                  message: "Если вам понравилось приложение, пожалуйста, оцените его в AppStore",
                                                  firstButtonTitle: "Оценить",
                                                  secondButtonTitle: "Отмена",
                                                  thirdButtonTitle: "Оценить",
                                                  firstButtonHandler: rateAction,
                                                  thirdButtonHandler: rateAction)
        viewController.present(alert, animated: true)
    }
    
    class func showFeedbackAlert(from viewController: UIViewController) {
        let rateAction: AlertButtonAction = { (_) in
            let url = URL(string: "https://itunes.apple.com/us/app/appName/id1357370653?mt=8&action=write-review")
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
            Reports.logEvent("settings_feedback_appstore", parameters: nil)
        }
        let alert = AlertHelper.alertThreeButtons(title: "Оценить",
                                                  message: "Если у Вас что-то произошло или сломалось, пишите нам в группу ВК. А если у Вас все хорошо - оставте, пожалуйста, отзыв в App Store",
                                                  firstButtonTitle: "Оценить",
                                                  secondButtonTitle: "Отмена",
                                                  thirdButtonTitle: "Группа ВК",
                                                  firstButtonHandler: rateAction,
                                                  thirdButtonHandler: { (_) in
                                                    let url = URL(string: "https://vk.com/voicestickersios")
                                                    UIApplication.shared.open(url!, options: [:], completionHandler: nil)
                                                    Reports.logEvent("settings_feedback_vk", parameters: nil)
        })
        
        viewController.present(alert, animated: true)
    }
    
    class func showAboutAlert(from viewController: UIViewController) {
        let applicationHelper = ApplicationHelper()
        let alert = AlertHelper.alert(title: "О приложении",
                                      message: "Если у вас есть идеи или предложения или у вас что-то не работает, напишите, пожалуйста, об этом в ВК @VoiceStickersIos. Оперативная поддержка. Версия \(applicationHelper.getApplicationVersion())",
            leftButtonTitle: "Группа ВК",
            rightButtonTitle: "Отмена",
            leftButtonHandler: { (_) in
                let url = URL(string: "https://vk.com/voicestickersios")
                UIApplication.shared.open(url!, options: [:], completionHandler: nil)
                Reports.logEvent("settings_about_vk", parameters: nil)
        }) { (_) in}
        viewController.present(alert, animated: true)
    }
    
    class func showNoVotesAlert(from viewController: UIViewController) {
        let alert = AlertHelper.alert(title: "Хватит!", message: "На сегодня желания закончились. Таблица голосования обновится завтра!", leftButtonTitle: "Ну лан(", rightButtonTitle: nil, leftButtonHandler: { (_) in
        }) { (_) in
        }
        viewController.present(alert, animated: true)
        Reports.logEvent("dream_no_votes_remaining", parameters: nil)
    }
    
    class func showDidBuyAlert(from viewController: UIViewController) {
        let alert = AlertHelper.alert(title: "Успешно", message: "Приобретен ВИП статус!", leftButtonTitle: "Ок", rightButtonTitle: nil, leftButtonHandler: { (_) in
        }, rightButtonHandler: { (_) in
        })
        viewController.present(alert, animated: true)
    }
    
    class func showCantChangeSticker(from viewController: UIViewController) {
        let alert = AlertHelper.alertOneButton(title: "Ошибка",
                                               message: "Не удалось обработать измененный стикер. Изменитте, пожалуйста, настройки скорости и тембра!",
                                               buttonTitle: "Ну ладно((")
        viewController.present(alert, animated: true)
    }
    
    class func showDesignAlert(from viewController: UIViewController) {
        let alert = AlertHelper.alert(title: "Помочь с дизайном",
                                      message: "Если вы умеете разрабатывать UX/UI и у вас есть идеи дизайна для данного приложения - то с нетерпением ждем вашего сообщения в группе ВК",
            leftButtonTitle: "Группа ВК",
            rightButtonTitle: "Отмена",
            leftButtonHandler: { (_) in
                let url = URL(string: "https://vk.com/voicestickersios")
                UIApplication.shared.open(url!, options: [:], completionHandler: nil)
                Reports.logEvent("settings_design_vk", parameters: nil)
        }) { (_) in}
        viewController.present(alert, animated: true)
    }
}

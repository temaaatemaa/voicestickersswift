//
//  SendView.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 17/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit

protocol SendViewDelegate {
    func didTapVkButton()
    func didTapTelegramButton()
    func didTapChangeButton()
}

class SendView: UIView {

    @IBOutlet weak var vkButton: UIButton!
    @IBOutlet weak var telegramButton: UIButton!
    @IBOutlet weak var changeButton: UIButton!
    
    var delegate: SendViewDelegate?
    
    class func initFromNib(with delegate: SendViewDelegate) -> SendView {
        let view = UINib(nibName: String(describing: SendView.self), bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! SendView
        view.delegate = delegate
        view.setup()
        return view
    }
    
    func setup() {
        vkButton.addTarget(self, action: #selector(didTapVkButton), for: .touchUpInside)
        telegramButton.addTarget(self, action: #selector(didTapTelegramButton), for: .touchUpInside)
        changeButton.addTarget(self, action: #selector(didTapChangeButton), for: .touchUpInside)
        
        let changeIcon = changeButton.image(for: .normal)?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        changeButton.setImage(changeIcon, for: .normal)
        changeButton.imageEdgeInsets = UIEdgeInsets(top: 3, left: 3, bottom: 3, right: 3)
        
        self.backgroundColor = UIColor(rgb: 0x34495e)
        self.layer.cornerRadius = 10
    }
    
    @objc func didTapVkButton() {
        self.delegate?.didTapVkButton()
    }
    
    @objc func didTapTelegramButton() {
        self.delegate?.didTapTelegramButton()
    }
    
    @objc func didTapChangeButton() {
        self.delegate?.didTapChangeButton()
    }
}

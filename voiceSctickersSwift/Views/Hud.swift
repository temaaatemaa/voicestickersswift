//
//  Hud.swift
//  voiceSctickersSwift
//
//  Created by Artem Zabludovsky on 16/05/2019.
//  Copyright © 2019 n. All rights reserved.
//

import UIKit
import JGProgressHUD

class Hud {
    static var loading: JGProgressHUD {
        get {
            let hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Загрузка"
            return hud
        }
    }
}
